package transporter.transporter_network.tools;

import org.junit.Test;

import transporter.transporter_network.models.User;

import static org.junit.Assert.assertNotNull;

public class CoreAdapterTest {

    @Test
    public void CoreAdapter_registerUser_GetResult_ValidInput() throws Exception {
        User user = new User("12301010101", "09/09/1999", "1234");
        user.setLanguage("ENGLISH");

        String result =  CoreAdapter.registerUser(user);
        assertNotNull(result);
    }
}