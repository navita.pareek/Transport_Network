package transporter.transporter_network.tools;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class CoreClientTest {

    @Test
    public void CoreClient_SimplePostConnection_GetResult_ValidInput() throws Exception {
        String result = (new CoreClient()).SimplePostConnection("http://localhost:8080/user/0/language/en", "");
        assertNotNull(result);
    }

    @Test
    public void CoreClient_SimpleGetConnection_GetResult_ValidInput() throws Exception {
        String result = (new CoreClient()).SimpleGetConnection("http://www.jsontest.com/");
        assertNotNull(result);
    }

    @Test
    public void CoreClient_SimplePostConnection_GetResult_ValidPostBodyInput() throws Exception {
        String result = (new CoreClient()).SimplePostConnection("http://localhost:8080/user/register", "{\"phoneNumber\":\"10101010101\",\"name\":\"test\",\"dateOfBirth\":\"09/09/1999\",\"password\":\"1234\",\"language\":\"ENGLISH\"}");
        assertNotNull(result);
    }
}