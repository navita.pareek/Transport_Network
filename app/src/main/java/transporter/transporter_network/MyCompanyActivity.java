package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.User;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class MyCompanyActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_company);

        (new getIsUserEmpower(MyCompanyActivity.this)).execute();
        getPreferredLanguage();
        getCompanyMembersList();
        setRemoveCompanyButtonText();
        setAddManpowerLabel();
        setRemoveManpowerLabel();
        setCompanyName();
    }

    @Override
    public void onResume(){
        super.onResume();
        getCompanyMembersList();
        setCompanyName();
    }

    private void setRemoveCompanyButtonText() {
        Button button = (Button) findViewById(R.id.button_update_company);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().edit_company_name_button_label));

        button = (Button) findViewById(R.id.button_remove_company);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().remove_company_label));
    }

    public void OnEditCompanySubmitClicked(View view){
        Intent intent = new Intent(MyCompanyActivity.this, EditCompanyNameActivity.class);
        startActivity(intent);
    }

    public void OnRemoveCompanySubmitClicked(View view){
        try {

            AlertDialog.Builder builder = new AlertDialog.Builder(MyCompanyActivity.this);
            builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().remove_company_confirmation_msg));
            builder.setCancelable(false);
            builder.setNegativeButton(
                    TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder.setPositiveButton(
                    TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            try {
                                removeCompany();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
        }
    }

    private void removeCompany() throws Exception {
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new MyCompanyActivity.removeCompany(MyCompanyActivity.this)).execute(user);
    }

    private void setCompanyName(){
        TextView companyName = (TextView) findViewById(R.id.TextView_my_company_name_label);
        companyName.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().company_name_short_label));

        TextView text = (TextView) findViewById(R.id.TextView_my_company_name);
        text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_COMPANY_NAME));
    }

    private void hideActionButtonsIfRequired() {
        if(!TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_IS_EMPOWERED).equals("true")){
            Button actionButton = (Button) findViewById(R.id.button_add_manpower);
            actionButton.setVisibility(View.GONE);
            actionButton = (Button) findViewById(R.id.button_remove_manpower);
            actionButton.setVisibility(View.GONE);
            actionButton = (Button) findViewById(R.id.button_update_company);
            actionButton.setVisibility(View.GONE);
        }else{
            Button actionButton = (Button) findViewById(R.id.button_add_manpower);
            actionButton.setVisibility(View.VISIBLE);
            actionButton = (Button) findViewById(R.id.button_remove_manpower);
            actionButton.setVisibility(View.VISIBLE);
            actionButton = (Button) findViewById(R.id.button_update_company);
            actionButton.setVisibility(View.VISIBLE);
        }
    }

    public void onRemoveManpowerClick(View view){
        Intent intent = new Intent(MyCompanyActivity.this, RemoveManpowerActivity.class);
        startActivity(intent);
    }

    public void OnAddManpowerClick(View view){
        Intent intent = new Intent(MyCompanyActivity.this, AddManpowerActivity.class);
        startActivity(intent);
    }

    private void getCompanyMembersList() {
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new getUserCompanyMembers(MyCompanyActivity.this)).execute(user);
    }

    private void setCompanyMembersListLabel() {
        TextView text = (TextView) findViewById(R.id.TextView_my_company_members_list_label);
        if(SeedData.getInstance().companyMembers.size() == 0) {
            text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no_company_members_added));
        }
        else{
            String VehicleCountMessage = SeedData.getInstance().companyMembers.size() + " " + TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().company_members_added);
            text.setText(VehicleCountMessage);
        }
    }

    private void setCompanyMembersList() {
        TextView text = (TextView) findViewById(R.id.TextView_my_company_members);
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < SeedData.getInstance().companyMembers.size(); i++){
            result.append((i+1) + ". ");
            result.append(SeedData.getInstance().companyMembers.get(i) + "\n");
        }
        text.setText(result.toString());
    }

    private void setRemoveManpowerLabel() {
        Button button = (Button) findViewById(R.id.button_remove_manpower);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().remove_manpower_label));
    }

    private void setAddManpowerLabel() {
        Button button = (Button) findViewById(R.id.button_add_manpower);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().add_man_power_label));
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private class getUserCompanyMembers extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getUserCompanyMembers(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.getUserCompanyMembers(user[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().COMPANY_MEMBERS_KEY, result);
                SeedData.getInstance().populateCompanyMembers();
                setCompanyMembersListLabel();
                setCompanyMembersList();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class removeCompany extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private removeCompany(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... users) {
            String result = null;
            for(int index = 0; index < users.length; index++){
                result = CoreAdapter.removeUserCompany(users[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("TRUE")){
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
                TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_COMPANY_NAME,null);
                Intent intent = new Intent(MyCompanyActivity.this, DashboardActivity.class);
                startActivity(intent);
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class getIsUserEmpower extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getIsUserEmpower(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getIsUserEmpowered(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_IS_EMPOWERED, result);
                hideActionButtonsIfRequired();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }
}
