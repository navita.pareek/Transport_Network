package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserVehicle;
import transporter.transporter_network.models.Vehicle;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class AddVehicleActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);

        getPreferredLanguage();
        updateRegisterFormLabelText();
        updateVehicleNumberText();
        updateCapacityHint();
        updateStartStateSpinner();
        initCheckBoxesText();
        initStartStateListener();
        initStartCityListener();
        initEndCityListener();
        initEndStateListener();
        updateStartCitySpinner();
        updateEndStateSpinner();
        updateEndCitySpinner();
        updateStatusSpinner();
        updateAddVehicleButtonText();
    }

    private void initCheckBoxesText() {
        ((CheckBox)findViewById(R.id.checkBox_any_start_state)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_start_state));
        ((CheckBox)findViewById(R.id.checkBox_any_start_city)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_start_city));
        ((CheckBox)findViewById(R.id.checkBox_any_end_state)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_end_state));
        ((CheckBox)findViewById(R.id.checkBox_any_end_city)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_end_city));
    }

    private void initEndCityListener() {
        CheckBox endCity = (CheckBox)findViewById(R.id.checkBox_any_end_city);
        endCity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner endCitySpinner = (Spinner) findViewById(R.id.spinner_end_city);
                if(isChecked) {
                    endCitySpinner.setSelection(0);
                    endCitySpinner.setEnabled(false);
                }else{
                    endCitySpinner.setEnabled(true);
                }
            }
        });
    }

    private void initStartCityListener() {
        CheckBox startCity = (CheckBox)findViewById(R.id.checkBox_any_start_city);
        startCity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner startCitySpinner = (Spinner) findViewById(R.id.spinner_start_city);
                if(isChecked) {
                    startCitySpinner.setSelection(0);
                    startCitySpinner.setEnabled(false);
                }else{
                    startCitySpinner.setEnabled(true);
                }
            }
        });
    }

    private void initStartStateListener() {
        CheckBox startState = (CheckBox)findViewById(R.id.checkBox_any_start_state);
        startState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner startStateSpinner = (Spinner) findViewById(R.id.spinner_start_state);
                Spinner startCitySpinner = (Spinner) findViewById(R.id.spinner_start_city);
                CheckBox startCityCheckbox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                if(isChecked) {
                    startStateSpinner.setSelection(0);
                    startStateSpinner.setEnabled(false);
                    startCityCheckbox.setChecked(false);
                    startCityCheckbox.setEnabled(false);
                    startCitySpinner.setSelection(0);
                    startCitySpinner.setEnabled(false);
                }else{
                    startStateSpinner.setEnabled(true);
                    startCitySpinner.setEnabled(true);
                    startCityCheckbox.setEnabled(true);
                }
            }
        });
    }

    private void initEndStateListener() {
        CheckBox endState = (CheckBox)findViewById(R.id.checkBox_any_end_state);
        endState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner endStateSpinner = (Spinner) findViewById(R.id.spinner_end_state);
                Spinner endCitySpinner = (Spinner) findViewById(R.id.spinner_end_city);
                CheckBox endCityCheckbox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                if(isChecked) {
                    endStateSpinner.setSelection(0);
                    endStateSpinner.setEnabled(false);
                    endCityCheckbox.setChecked(false);
                    endCityCheckbox.setEnabled(false);
                    endCitySpinner.setSelection(0);
                    endCitySpinner.setEnabled(false);
                }else{
                    endStateSpinner.setEnabled(true);
                    endCitySpinner.setEnabled(true);
                    endCityCheckbox.setEnabled(true);
                }
            }
        });
    }

    private void updateVehicleNumberText() {
        TextView textView = (TextView) findViewById(R.id.textView_vehicle_number_label);
        textView.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().vehicle_number_label));
    }

    private void updateCapacityHint() {
        TextView textView = (TextView) findViewById(R.id.textView_vehicle_capacity_label);
        textView.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().vehicle_capacity_label));
    }

    private void updateStartCitySpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_start_city);
        String currentState = ((Spinner) findViewById(R.id.spinner_start_state)).getSelectedItem().toString();

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_start_city));

        if(!currentState.equals(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_start_state)))
            spinnerArray.addAll(SeedData.getInstance().stateCity.get(currentState));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
    }

    private void updateStartStateSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_start_state);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_start_state));

        for(Map.Entry<String, List<String>> entry : SeedData.getInstance().stateCity.entrySet()){
            spinnerArray.add(entry.getKey());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if(arg2 == 0){
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_start_city);
                    spinner.setVisibility(View.GONE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                    checkBox.setVisibility(View.GONE);
                }
                else{
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_start_city);
                    spinner.setVisibility(View.VISIBLE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                    checkBox.setVisibility(View.VISIBLE);
                }
                updateStartCitySpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void updateEndCitySpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
        String currentState = ((Spinner) findViewById(R.id.spinner_end_state)).getSelectedItem().toString();

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_city));

        if(!currentState.equals(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_state)))
            spinnerArray.addAll(SeedData.getInstance().stateCity.get(currentState));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
    }

    private void updateEndStateSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_end_state);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_state));

        for(Map.Entry<String, List<String>> entry : SeedData.getInstance().stateCity.entrySet()){
            spinnerArray.add(entry.getKey());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if(arg2 == 0){
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
                    spinner.setVisibility(View.GONE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                    checkBox.setVisibility(View.GONE);
                }
                else{
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
                    spinner.setVisibility(View.VISIBLE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                    checkBox.setVisibility(View.VISIBLE);
                }
                updateEndCitySpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void updateStatusSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_current_vehicle_status);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_vehicle_status));
        for(int index = 0; index < SeedData.getInstance().vehicleStatus.size(); index++) {
            spinnerArray.add(SeedData.getInstance().vehicleStatus.get(index));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
    }

    private void updateAddVehicleButtonText() {
        Button button = (Button)findViewById(R.id.button_add_vehicle);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().add_vehicle_label));
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void updateRegisterFormLabelText() {
        TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
        registerMeButton.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_add_vehicle_form_label));
        registerMeButton.setTextColor(Color.BLACK);
    }

    private boolean allUserInputsAreValid() {
        TextView textView = (TextView)findViewById(R.id.TextView_search_result_label);

        if(!areAllRequiredFieldsFilled()){
            textView.setText(LanguageStrings.getInstance().fill_required_field_label.get(preferredLanguage));
            textView.setTextColor(Color.RED);
            return false;
        }

        if(!isValidVehicleNumber()){
            textView.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().invalid_vehicle_number_label));
            textView.setTextColor(Color.RED);
            return false;
        }

        if(!isValidVehicleCapacity()){
            textView.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().invalid_capacity_label));
            textView.setTextColor(Color.RED);
            return false;
        }

        return true;
    }

    private boolean isValidVehicleNumber() {
        EditText text = (EditText) findViewById(R.id.editText_vehicle_number);
        return text.getText().toString().trim().length() > 3;
    }

    private boolean isValidVehicleCapacity() {
        EditText text = (EditText) findViewById(R.id.editText_vehicle_capacity);
        return text.getText().toString().trim().length() > 0;
    }

    private boolean areAllRequiredFieldsFilled() {
        return (((EditText) findViewById(R.id.editText_vehicle_number)).getText().toString().trim().length() != 0 &&
                ((EditText) findViewById(R.id.editText_vehicle_capacity)).getText().toString().trim().length() != 0 );
    }

    public void OnAddVehicleSubmitClicked(View view) {
        if(allUserInputsAreValid()){
            try {
                addVehicle();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void addVehicle() throws Exception {
        updateRegisterFormLabelText();
        String vehicleNumber = ((EditText) findViewById(R.id.editText_vehicle_number)).getText().toString();
        String vehicleCapacityInTon = ((EditText) findViewById(R.id.editText_vehicle_capacity)).getText().toString();
        int startState = ((Spinner) findViewById(R.id.spinner_start_state)).getSelectedItemPosition();
        int startCity = ((Spinner) findViewById(R.id.spinner_start_city)).getSelectedItemPosition();
        int endState = ((Spinner) findViewById(R.id.spinner_end_state)).getSelectedItemPosition();
        int endCity = ((Spinner) findViewById(R.id.spinner_end_city)).getSelectedItemPosition();
        int status = ((Spinner) findViewById(R.id.spinner_current_vehicle_status)).getSelectedItemPosition();
        Boolean isLinkedToCompany = true;

        Vehicle vehicle = new Vehicle(vehicleNumber, vehicleCapacityInTon);
        vehicle.setStartState(Integer.toString(startState));
        vehicle.setStartCity(Integer.toString(startCity));
        vehicle.setEndState(Integer.toString(endState));
        vehicle.setEndCity(Integer.toString(endCity));
        vehicle.setStatus(Integer.toString(status));
        vehicle.setIsLinkedToCompany(isLinkedToCompany);

        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        UserVehicle userVehicle = new UserVehicle(user, vehicle);

        (new addVehicle(AddVehicleActivity.this)).execute(userVehicle);
    }

    private class addVehicle extends AsyncTask<UserVehicle, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private addVehicle(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserVehicle... userVehicle) {
            String result = null;
            for(int index = 0; index < userVehicle.length; index++){
                result = CoreAdapter.addUserVehicle(userVehicle[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("TRUE")){
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();

                ((EditText) findViewById(R.id.editText_vehicle_number)).setText("");
                ((EditText) findViewById(R.id.editText_vehicle_capacity)).setText("");
                ((Spinner) findViewById(R.id.spinner_start_state)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_start_city)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_end_state)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_end_city)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_current_vehicle_status)).setSelection(0);

            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else if(result.contains(LanguageStrings.getInstance().BAD_REQUEST)){
                    builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().bad_request_toast));
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
