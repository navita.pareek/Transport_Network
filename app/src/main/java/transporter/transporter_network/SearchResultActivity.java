package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.Vehicle;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class SearchResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        setSearchResultTitle();
        setSearchResult();
    }

    private void setSearchResultTitle() {
        TextView title = (TextView) findViewById(R.id.TextView_disclaimer_label);
        title.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().title_disclaimer));

        title = (TextView) findViewById(R.id.TextView_search_result_label);
        title.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().title_activity_search_result));
    }

    private void setSearchResult() {
        String result = getIntent().getStringExtra(CoreConstants.SearchVehicleResult);

        if(result.length() == 0){
            updateTitleForEmptyResult();
            return;
        }

        try {
            JSONObject mainJsonObject = new JSONObject(result);
            JSONArray interimArray = mainJsonObject.getJSONArray(CoreConstants.users);

            if(interimArray.length() == 0){
                updateTitleForEmptyResult();
                return;
            }

            LinearLayout layout = (LinearLayout) findViewById(R.id.linearLayout_call);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(20,10,20,10);
            StringBuilder stringBuilder = new StringBuilder();

            for (int index = 0; index < interimArray.length(); index++) {
                JSONObject jsonObject = interimArray.getJSONObject(index);
                String phoneNumber = jsonObject.getString("phoneNumber");
                String companyName = jsonObject.getString("companyName");
                String userTnPoints = jsonObject.getString("userTnPoints");

                if(stringBuilder.indexOf(phoneNumber) < 0){
                    stringBuilder.append(":" + phoneNumber + ":");

                    Button btnTag = new Button(this);
                    btnTag.setCompoundDrawablesWithIntrinsicBounds( android.R.drawable.sym_action_call, 0, 0, 0);
                    btnTag.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    btnTag.setText("( " + companyName + " ) " + phoneNumber + " : " + userTnPoints);
                    btnTag.setContentDescription(phoneNumber);
                    btnTag.setBackgroundColor(getResources().getColor(R.color.black_overlay));

                    btnTag.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Button b = (Button)view;

                            String callerphoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
                            String calledPhoneNumber = b.getContentDescription().toString();
                            User user = new User(callerphoneNumber);
                            user.setReferencePhoneNumber(calledPhoneNumber);
                            (new SearchResultActivity.userContacted(SearchResultActivity.this)).execute(user);

                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", calledPhoneNumber, null));
                            startActivity(intent);
                        }
                    });

                    LinearLayout row = new LinearLayout(this);
                    row.setLayoutParams(params);
                    row.addView(btnTag);

                    layout.addView(row);
                }
            }
        }catch (Exception ex){
            System.out.println(ex);
        }
    }

    private void updateTitleForEmptyResult() {
        (findViewById(R.id.TextView_disclaimer_label)).setVisibility(View.GONE);
        TextView title = (TextView) findViewById(R.id.TextView_search_result_label);
        title.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no_result_found));
    }

    private class userContacted extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private userContacted(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.userContacted(user[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }
}
