package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.User;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class MyVehiclesActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_vehicles);

        getPreferredLanguage();
        getVehicleList();
        setAddVehicleLabel();
        setRemoveVehicleLabel();
        setUpdateTruckStatus();
        setSearchLoadForVehicleButtonLabel();
    }

    @Override
    public void onResume(){
        super.onResume();
        getVehicleList();
    }

    private void setSearchLoadForVehicleButtonLabel() {
        Button button = (Button) findViewById(R.id.button_search_load_for_added_vehicles);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().search_load_for_added_vehicles_label));
    }

    public void OnSearchLoadForVehicleClick(View view){
        Intent intent = new Intent(MyVehiclesActivity.this, MyWorkActivity.class);
        intent.putExtra(CoreConstants.SearchEntity, CoreConstants.SearchLoadForVehicle);
        startActivity(intent);
    }

    private void setUpdateTruckStatus() {
        Button button = (Button) findViewById(R.id.button_update_truck_status);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().update_truck_status_label));
    }

    public void onRemoveVehicleClicked(View view){
        Intent intent = new Intent(MyVehiclesActivity.this, RemoveVehicleActivity.class);
        startActivity(intent);
    }

    public void OnUpdateVehicleStatusClick(View view){
        Intent intent = new Intent(MyVehiclesActivity.this, UpdateVehicleStatusActivity.class);
        startActivity(intent);
    }

    public void OnAddVehicleClick(View view){
        Intent intent = new Intent(MyVehiclesActivity.this, AddVehicleActivity.class);
        startActivity(intent);
    }

    private void getVehicleList() {
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new MyVehiclesActivity.getUserVehicle(MyVehiclesActivity.this)).execute(user);
    }

    private void setVehicleListLabel() {
        TextView text = (TextView) findViewById(R.id.TextView_my_vehicles_list_label);
        if(SeedData.getInstance().vehicles.size() == 0) {
            text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no_vehicle_added));
        }
        else{
            String VehicleCountMessage = SeedData.getInstance().vehicles.size() + " " + TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().vehicle_added);
            text.setText(VehicleCountMessage);
        }
    }

    private void setVehicleList() {
        TextView text = (TextView) findViewById(R.id.TextView_my_vehicles);
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < SeedData.getInstance().vehicles.size(); i++){
            result.append((i+1) + ".\n");
            result.append(SeedData.getInstance().vehicles.get(i).getVehicleSummary(preferredLanguage) + "\n");
        }
        text.setText(result.toString());
    }

    private void setRemoveVehicleLabel() {
        Button button = (Button) findViewById(R.id.button_remove_vehicle);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().remove_vehicle));
    }

    private void setAddVehicleLabel() {
        Button button = (Button) findViewById(R.id.button_add_truck);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().add_vehicle_label));
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void hideActionButtonsIfVehicleListEmpty() {
        if(SeedData.getInstance().vehicles.size() == 0) {
            Button button = (Button) findViewById(R.id.button_update_truck_status);
            button.setVisibility(View.GONE);
            button = (Button) findViewById(R.id.button_search_load_for_added_vehicles);
            button.setVisibility(View.GONE);
            button = (Button) findViewById(R.id.button_remove_vehicle);
            button.setVisibility(View.GONE);
        }
        else{
            Button button = (Button) findViewById(R.id.button_update_truck_status);
            button.setVisibility(View.VISIBLE);
            button = (Button) findViewById(R.id.button_search_load_for_added_vehicles);
            button.setVisibility(View.VISIBLE);
            button = (Button) findViewById(R.id.button_remove_vehicle);
            button.setVisibility(View.VISIBLE);
        }
    }

    private class getUserVehicle extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;
        private String userPhoneNumber;

        private getUserVehicle(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.getUserVehicle(user[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().VEHICLES_KEY, result);
                SeedData.getInstance().populateVehiclesMap();
                hideActionButtonsIfVehicleListEmpty();
                setVehicleListLabel();
                setVehicleList();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
