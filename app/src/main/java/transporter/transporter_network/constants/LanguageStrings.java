package transporter.transporter_network.constants;

import java.util.HashMap;
import java.util.Map;

import transporter.transporter_network.tools.TransporterSharedPreferences;

public class LanguageStrings {

    public static final String BAD_REQUEST = "Bad Request";
    public static final String CUSTOMER_SUPPORT = "CUSTOMER_SUPPORT";
    public static final String HAPPY = "HAPPY";
    public static final String NEED_IMPROVEMENT = "NEED_IMPROVEMENT";
    public static final String FORGET_PASSWORD = "FORGET_PASSWORD";
    public static final String NEW_CUSTOMER_SUPPORT = "NEW_CUSTOMER_SUPPORT";
    private static LanguageStrings instance = null;

    public final String LANGUAGE_PREFERRED_KEY = "LANGUAGE_PREFERRED";
    public final String STATE_CITY_KEY = "STATE_CITY";
    public final String VEHICLES_KEY = "VEHICLES";
    public final String TRIPS_KEY = "TRIPS";
    public final String VEHICLE_STATUS_KEY = "VEHICLE_STATUS";
    public final String TRIP_STATUS_KEY = "TRIP_STATUS";
    public final String CONSTANTS_KEY = "CONSTANTS_LENGTH";
    public static final String COMPANY_MEMBERS_KEY = "COMPANY_MEMBERS_KEY";

    public final static String HINDI_KEY = "HINDI";
    public final static String ENGLISH_KEY = "ENGLISH";

    public Map<String,String> open_register_user_activity_label = new HashMap<>();
    public Map<String,String> chose_preferred_language_label = new HashMap<>();
    public Map<String,String> fill_register_form_label = new HashMap<>();
    public Map<String,String> fill_login_form_label = new HashMap<>();
    public Map<String,String> phone_number_label = new HashMap<>();
    public Map<String,String> phone_number_hint = new HashMap<>();
    public Map<String,String> forget_password = new HashMap<>();
    public Map<String,String> password_label = new HashMap<>();
    public Map<String,String> confirm_password_label = new HashMap<>();
    public Map<String,String> register_user_label = new HashMap<>();
    public Map<String,String> invalid_phone_number_label = new HashMap<>();
    public Map<String,String> invalid_password_confirm_password_label = new HashMap<>();
    public Map<String,String> invalid_information_label = new HashMap<>();
    public Map<String,String> connection_error_toast = new HashMap<>();
    public Map<String,String> welcome_message = new HashMap<>();
    public Map<String,String> registration_failed_msg = new HashMap<>();
    public Map<String,String> ok_label = new HashMap<>();
    public Map<String,String> request_succeeded_toast = new HashMap<>();
    public Map<String,String> HINDI_Error_Code_Map = new HashMap<>();
    public Map<String,String> ENGLISH_Error_Code_Map = new HashMap<>();
    public Map<String,String> new_user_login_button_label = new HashMap<>();
    public Map<String,String> login_button_label = new HashMap<>();
    public Map<String,String> main_page_language_set_to = new HashMap<>();
    public Map<String,String> dashboard_language_set_to = new HashMap<>();
    public Map<String,String> fill_required_field_label = new HashMap<>();
    public Map<String, String> invalid_password_label = new HashMap<>();
    public Map<String, String> old_password_label = new HashMap<>();
    public Map<String, String> change_password_label = new HashMap<>();
    public Map<String, String> forget_password_label = new HashMap<>();
    public Map<String, String> fill_change_pin_form_label = new HashMap<>();
    public Map<String, String> new_password_same_as_old_password_label = new HashMap<>();
    public Map<String, String> contact_support_text = new HashMap<>();
    public Map<String, String> disclaimer_message = new HashMap<>();
    public Map<String, String> reference_phone_number_label = new HashMap<>();
    public Map<String, String> points_rules = new HashMap<>();

    public Map<String, String> VEHICLE_NUMBER = new HashMap<>();
    public Map<String, String> START_LOCATION = new HashMap<>();
    public Map<String, String> END_LOCATION = new HashMap<>();
    public Map<String, String> VEHICLE_CAPACITY = new HashMap<>();
    public Map<String, String> STATUS = new HashMap<>();

    public HashMap LOAD_TRIP_DATE = new HashMap();

    public final String search_truck_label = "search_truck_label";
    public final String search_load_label = "search_load_label";
    public final String add_company_name_label = "add_company_name_label";
    public final String add_man_power_label = "add_man_power_label";
    public final String update_truck_status_label = "update_truck_status_label";
    public final String update_trip_status_label = "update_trip_status_label";
    public final String remove_vehicle = "remove_vehicle";
    public final String remove_manpower_label = "remove_manpower_label";
    public final String vehicle_number_label = "vehicle_number_label";
    public final String vehicle_capacity_label = "vehicle_capacity_label";
    public final String add_vehicle_label = "add_vehicle_label";
    public final String invalid_vehicle_number_label = "invalid_vehicle_number_label";
    public final String invalid_capacity_label = "invalid_capacity_label";
    public final String company_name_label = "company_name_label";
    public final String fill_update_vehicle_form_label = "fill_update_vehicle_form_label";
    public final String new_member_phone_number_label = "new_member_phone_number_label";
    public final String is_empower_to_change_structure = "is_empower_to_change_structure";
    public final String invalid_date_label = "invalid_date_label";
    public final String select_start_city = "select_start_city";
    public final String select_start_state = "select_start_state";
    public final String select_destination_city = "select_destination_city";
    public final String select_destination_state = "select_destination_state";
    public final String select_vehicle_current_city = "select_vehicle_current_city";
    public final String select_vehicle_current_state = "select_vehicle_current_state";
    public final String select_vehicle_number = "select_vehicle_number";
    public final String select_trip = "select_trip";
    public final String select_trip_status = "select_trip_status";
    public final String no_result_found = "no_result_found";
    public final String title_activity_search_result = "title_activity_search_result";
    public final String select_vehicle_status = "select_vehicle_status";
    public final String logout_label = "logout_label";
    public final String start_date_search_vehicle = "start_date_search_vehicle";
    public final String vehicle_capacity_required_label = "vehicle_capacity_required_label";
    public final String load_weight = "load_weight";
    public final String my_trips_label = "my_trips_label";
    public final String tonne = "tonne";
    public final String phone_number_to_remove = "phone_number_to_remove";
    public final String customer_happy = "customer_happy";
    public final String yes = "yes";
    public final String no = "no";
    public final String logout_confirmation = "logout_confirmation";
    public final String want_team_to_call = "want_team_to_call";
    public final String update_company_name_label = "update_company_name_label";
    public final String remove_company_label = "remove_company_label";
    public final String no_vehicle_added = "no_vehicle_added";
    public final String vehicle_added = "vehicle_added";
    public final String no_company_members_added = "no_company_members_added";
    public final String company_members_added = "company_members_added";
    public final String add_today_date_search_vehicle = "add_today_date_search_vehicle";
    public final String add_today_date_search_load = "add_today_date_search_load";
    public final String edit_company_name_label = "edit_company_name_label";
    public final String new_vehicle_capacity_label = "new_vehicle_capacity_label";
    public final String my_company_label = "my_company_label";
    public final String my_vehicles_label = "my_vehicles_label";
    public final String fill_company_name_label = "fill_company_name_label";
    public final String remove_company_confirmation_msg = "remove_company_confirmation_msg";
    public final String fill_add_vehicle_form_label = "fill_add_vehicle_form_label";
    public final String fill_remove_vehicle_label = "fill_remove_vehicle_label";
    public final String remove_vehicle_confirmation_msg = "remove_vehicle_confirmation_msg";
    public final String fill_search_vehicle_form_label = "fill_search_vehicle_form_label";
    public final String fill_search_load_form_label = "fill_search_load_form_label";
    public final String fill_add_member_form_label = "fill_add_member_form_label";
    public final String fill_remove_member_form_label = "fill_remove_member_form_label";
    public final String remove_company_member_confirmation_msg = "remove_company_member_confirmation_msg";
    public final String todays_date_added_as_start_date = "todays_date_added_as_start_date";
    public final String bad_request_toast = "bad_request_toast";
    public final String company_name_short_label = "company_name_short_label";
    public final String search_load_for_added_vehicles_label = "search_load_for_added_vehicles_label";
    public final String search_vehicle_for_added_trips_label = "search_vehicle_for_added_trips_label";
    public final String no_trip_added = "no_trip_added";
    public final String trips_added = "trips_added";
    public final String add_trip_label = "add_trip_label";
    public final String fill_add_trip_form_label = "fill_add_trip_form_label";
    public final String edit_company_name_button_label = "edit_company_name_button_label";
    public final String same_edit_company_name = "same_edit_company_name";
    public final String fill_form_update_trip_status_label = "fill_form_update_trip_status_label";
    public final String chkBox_any_start_state = "chkBox_any_start_state";
    public final String chkBox_any_start_city = "chkBox_any_start_city";
    public final String chkBox_any_end_state = "chkBox_any_end_state";
    public final String chkBox_any_end_city = "chkBox_any_end_city";
    public final String select_vehicle_from_spinner = "select_vehicle_from_spinner";
    public final String select_trip_from_spinner = "select_trip_from_spinner";
    public final String today_date_is_min_date = "today_date_is_min_date";
    public final String contact_user_support = "contact_user_support";
    public final String contact_support_label = "contact_support_label";
    public final String title_disclaimer = "title_disclaimer";
    public final String user_tn_points = "user_tn_points";
    public final String tn_point_definition = "tn_point_definition";

    private LanguageStrings(){
        populateRegisterMap();
        populateOpenRegisterFormMap();
        populateChooseLanguageMap();
        populateFillRegisterMap();
        populatePhoneNumberMap();
        populatePasswordMap();
        populateConfirmPasswordMap();
        populateInvalidDataMap();
        populateConnectionErrorToastMap();
        populateWelcomeMessageMap();
        populateRegistrationFailedMap();
        populateOkLabelMap();
        populateRequestSucceededMap();
        populateLoginLabelMap();
        populateHindiErrorCodeMap();
        populateEnglishErrorCodeMap();
        populateLanguageSetMsgMap();
        populateVehicleSummaryConstants();
    }

    private void populateVehicleSummaryConstants() {
        VEHICLE_NUMBER.put(HINDI_KEY, "वाहन संख्या");
        VEHICLE_NUMBER.put(ENGLISH_KEY, "Vehicle number");

        START_LOCATION.put(HINDI_KEY, "प्रारंभ स्थान");
        START_LOCATION.put(ENGLISH_KEY, "Start location");

        END_LOCATION.put(HINDI_KEY, "अंतिम स्थान");
        END_LOCATION.put(ENGLISH_KEY, "End location");

        VEHICLE_CAPACITY.put(HINDI_KEY, "वाहन क्षमता");
        VEHICLE_CAPACITY.put(ENGLISH_KEY, "Vehicle capacity");

        STATUS.put(HINDI_KEY, "स्थिति");
        STATUS.put(ENGLISH_KEY, "Status");

        LOAD_TRIP_DATE.put(HINDI_KEY, "यात्रा की तारीख");
        LOAD_TRIP_DATE.put(ENGLISH_KEY, "Trip date");
    }

    private void populateLanguageSetMsgMap() {
        main_page_language_set_to.put(HINDI_KEY, "सभी संदेश हिंदी में होंगे| रजिस्टर या लॉग इन करें|");
        main_page_language_set_to.put(ENGLISH_KEY, "All messages will be in english. Click below to Register/Login.");

        dashboard_language_set_to.put(HINDI_KEY, "सभी संदेश हिंदी में होंगे|");
        dashboard_language_set_to.put(ENGLISH_KEY, "All messages will be in english.");

        contact_support_text.put(HINDI_KEY, "ट्रांसपोर्ट नेटवर्क सहायता टीम संपर्क");
        contact_support_text.put(ENGLISH_KEY, "Contact Transport Network Support Team");
    }

    private void populateLoginLabelMap() {
        new_user_login_button_label.put(HINDI_KEY, "पहले से उपयोगकर्ता, यहाँ लॉग इन करें");
        new_user_login_button_label.put(ENGLISH_KEY, "Already Registered, Login here");
        login_button_label.put(HINDI_KEY, "लॉग इन");
        login_button_label.put(ENGLISH_KEY, "Login");
    }

    private void populateEnglishErrorCodeMap() {
        ENGLISH_Error_Code_Map.put("COMPANY_NOT_REGISTERED", "Company not registered.");
        ENGLISH_Error_Code_Map.put("NO_COMPANY_REGISTERED_TO_SHOW_MEMBERS", "No Company registered to show members. Add Company in \"MY COMPANY\" to use this feature.");
        ENGLISH_Error_Code_Map.put("COMPANY_REGISTERED", "Provided company is already registered. Ask company members to add you as member.");
        ENGLISH_Error_Code_Map.put("CONTACT_FOR_MANPOWER", "Contact given number and ask to add you as manpower");
        ENGLISH_Error_Code_Map.put("DUPLICATE_REQUEST", "Request already raised and succeeded.");
        ENGLISH_Error_Code_Map.put("REQUIRED_INPUTS_ARE_NOT_PROVIDED", "Required information are not provided.");
        ENGLISH_Error_Code_Map.put("SERVER_UNDER_MAINTENANCE", "Server is currently under maintenance.");
        ENGLISH_Error_Code_Map.put("UNABLE_TO_FETCH_DETAILS", "Unable to fetch details for processing request");
        ENGLISH_Error_Code_Map.put("USER_ALREADY_REGISTERED", "Provided user is already registered.");
        ENGLISH_Error_Code_Map.put("USER_NOT_ELIGIBLE_TO_ADD_MANPOWER", "You are not empowered to add or remove other manpower.");
        ENGLISH_Error_Code_Map.put("USER_NOT_REGISTERED", "Provided user is not registered. Go back to previous screen and first register as new user.");
        ENGLISH_Error_Code_Map.put("VEHICLE_ALREADY_REGISTERED", "Provided vehicle is already registered.");
        ENGLISH_Error_Code_Map.put("USER_IS_ALREADY_WORKING_FOR_SOME_COMPANY", "Requested new member is already working for some company.");
        ENGLISH_Error_Code_Map.put("USER_NOT_REGISTER_COMPANY", "No Company registered by you.");
        ENGLISH_Error_Code_Map.put("INVALID_INFORMATION_PROVIDED", "Provided information is incorrect");
        ENGLISH_Error_Code_Map.put("USER_NOT_IN_SAME_COMPANY", "Provided user is in different company.");
        ENGLISH_Error_Code_Map.put("VEHICLE_NOT_REGISTERED", "Provided vehicle is not registered.");
        ENGLISH_Error_Code_Map.put("TRIP_ALREADY_EXISTS", "Provided trip is already added.");
        ENGLISH_Error_Code_Map.put("NOT_CONNECTED_TO_INTERNET", "Phone is not connected to internet or having poor connection. Check mobile data.");
    }

    private void populateHindiErrorCodeMap() {
        HINDI_Error_Code_Map.put("COMPANY_NOT_REGISTERED", "कंपनी पंजीकृत नहीं है|");
        HINDI_Error_Code_Map.put("NO_COMPANY_REGISTERED_TO_SHOW_MEMBERS", "कोई कंपनी पंजीकृत नहीं है सदस्यों को दिखाने के लिए| इस सुविधा का उपयोग करने के लिए कंपनी \"मेरी कंपनी\" जोड़ें।");
        HINDI_Error_Code_Map.put("COMPANY_REGISTERED", "प्रदान की गई कंपनी पंजीकृत है| कंपनी के सदस्यों से आपको सदस्य के रूप में जोड़ने के लिए कहें|");
        HINDI_Error_Code_Map.put("CONTACT_FOR_MANPOWER", "दिए गए नंबर से संपर्क करें और आपको जनशक्ति के रूप में जोड़ने के लिए कहें|");
        HINDI_Error_Code_Map.put("DUPLICATE_REQUEST", "अनुरोध पहले ही उठाया गया है और सफल हुआ है|");
        HINDI_Error_Code_Map.put("REQUIRED_INPUTS_ARE_NOT_PROVIDED", "आवश्यक जानकारी प्रदान नहीं की गई है|");
        HINDI_Error_Code_Map.put("SERVER_UNDER_MAINTENANCE", "इस समय सर्वर धीमा है।");
        HINDI_Error_Code_Map.put("UNABLE_TO_FETCH_DETAILS", "अनुरोध के लिए जानकारी लाने में असमर्थ|");
        HINDI_Error_Code_Map.put("USER_ALREADY_REGISTERED", "उपलब्ध उपयोगकर्ता पहले ही पंजीकृत है|");
        HINDI_Error_Code_Map.put("USER_NOT_ELIGIBLE_TO_ADD_MANPOWER", "आप अन्य जनशक्ति को जोड़ने या निकालें के लिए सशक्त नहीं हैं|");
        HINDI_Error_Code_Map.put("USER_NOT_REGISTERED", "उपलब्ध उपयोगकर्ता पंजीकृत नहीं है| पिछली स्क्रीन पर वापस जाएं और पहले नए उपयोगकर्ता के रूप में पंजीकरण करें|");
        HINDI_Error_Code_Map.put("VEHICLE_ALREADY_REGISTERED", "उपलब्ध वाहन पहले ही पंजीकृत है|");
        HINDI_Error_Code_Map.put("USER_IS_ALREADY_WORKING_FOR_SOME_COMPANY", "अनुरोध किया गया नया सदस्य पहले से ही किसी कंपनी के लिए काम कर रहा है|");
        HINDI_Error_Code_Map.put("USER_NOT_REGISTER_COMPANY", "आपके द्वारा पंजीकृत कोई कंपनी नहीं है|");
        HINDI_Error_Code_Map.put("INVALID_INFORMATION_PROVIDED", "प्रदान की गई जानकारी गलत है|");
        HINDI_Error_Code_Map.put("USER_NOT_IN_SAME_COMPANY","दिया उपयोगकर्ता अलग कंपनी में है|");
        HINDI_Error_Code_Map.put("VEHICLE_NOT_REGISTERED","दिया वाहन पंजीकृत नहीं है|");
        HINDI_Error_Code_Map.put("TRIP_ALREADY_EXISTS", "प्रदान की गई यात्रा पहले ही जोड़ दी गई है।");
        HINDI_Error_Code_Map.put("NOT_CONNECTED_TO_INTERNET", "फोन इंटरनेट से जुड़ा नहीं है या खराब कनेक्शन है। मोबाइल डेटा जांचें");

    }

    public void populateConstantsMap(String inputString){
        if(inputString == null)
            return;

        String[] interimArray = inputString.split(",");
        for(int index = 0; index < interimArray.length; index++){
            String[] temp = interimArray[index].split(":");
            if(temp.length > 1) {
                TransporterSharedPreferences.getInstance().setString(temp[0], temp[1]);
            }
        }
        TransporterSharedPreferences.getInstance().setString(CONSTANTS_KEY, "PRESENT");
    }

    private void populateRequestSucceededMap() {
        request_succeeded_toast.put(HINDI_KEY, "अनुरोध सफल");
        request_succeeded_toast.put(ENGLISH_KEY, "Request Succeeded.");
    }

    private void populateOkLabelMap() {
        ok_label.put(HINDI_KEY, "ठीक है");
        ok_label.put(ENGLISH_KEY, "Ok");
    }

    private void populateRegistrationFailedMap() {
        registration_failed_msg.put(HINDI_KEY, "उपयोगकर्ता पंजीकरण विफल| निम्नलिखित चीजों की जांच करें,\n1. नेटवर्क कनेक्शन\n2. क्या उपयोगकर्ता पहले ही पंजीकृत है?");
        registration_failed_msg.put(ENGLISH_KEY, "User Registration Failed. Check following things,\n1. Network Connection.\n2. Is User already Registered?");
    }

    private void populateWelcomeMessageMap() {
        welcome_message.put(HINDI_KEY, "ट्रांसपोर्ट नेटवर्क में आपका स्वागत है\n1. भार के लिए परिवहन वाहन खोजें|\n2. वाहन के लिए उपलब्ध लोड खोजें|\n3. वाहन की जानकारी जोड़कर ट्रांसपोर्टरों से कनेक्ट करें|\n4. लोड की जानकारी जोड़कर वाहन मालिकों से कनेक्ट करें|\n 5. अपनी कंपनी के सदस्यों से जुड़ें|");
        welcome_message.put(ENGLISH_KEY, "Welcome to Transport Network.\n1. Search transportation vehicle for load.\n2. Search available load for vehicle.\n3. Connect to transporters by adding vehicle details.\n4. Connect to vehicle owners by adding load information.\n5. Connect with your company members.");

        disclaimer_message.put(HINDI_KEY, "**ट्रांसपोर्ट नेटवर्क किसी भी ट्रांसपोर्टर या वाहन के मालिक से संबंधित नहीं है| कृपया व्यापार समझौते में प्रवेश करने से पहले पार्टी की पृष्ठभूमि और भुगतान की जांच करें।**");
        disclaimer_message.put(ENGLISH_KEY, "**Transport Network is not directly related to any transporter or vehicle owner. Kindly check party's background and payment before entering into business deal.**");
    }

    private void populateConnectionErrorToastMap() {
        connection_error_toast.put(HINDI_KEY, "अनुरोध विफल, नेटवर्क कनेक्शन की जांच करें");
        connection_error_toast.put(ENGLISH_KEY, "Request Failed, check network connection");
    }

    private void populateInvalidDataMap() {
        invalid_password_confirm_password_label.put(HINDI_KEY, "प्रदान किए गए दोनों पासवर्ड की जांच करें");
        invalid_password_confirm_password_label.put(ENGLISH_KEY, "Provide Password and same confirm password of size 6.");

        invalid_password_label.put(HINDI_KEY, "6 अंकों के पासवर्ड प्रदान करें");
        invalid_password_label.put(ENGLISH_KEY, "Provide Password of 6 digits.");

        invalid_information_label.put(HINDI_KEY, "प्रदान की गई जानकारी गलत है");
        invalid_information_label.put(ENGLISH_KEY, "Provided information is incorrect");

        invalid_phone_number_label.put(HINDI_KEY, "आपका 10 अंकों का फोन नंबर दिजिये");
        invalid_phone_number_label.put(ENGLISH_KEY, "Provided your 10 digit Phone number.");
    }

    private void populateConfirmPasswordMap() {
        confirm_password_label.put(HINDI_KEY, "पिन की पुष्टि कीजिये");
        confirm_password_label.put(ENGLISH_KEY, "Confirm PIN");
    }

    private void populatePasswordMap() {
        password_label.put(HINDI_KEY, "पिन 6 लंबाई की");
        password_label.put(ENGLISH_KEY, "PIN of 6 digit");

        old_password_label.put(HINDI_KEY, "पुरानी पिन");
        old_password_label.put(ENGLISH_KEY, "Old PIN");

        change_password_label.put(HINDI_KEY, "पिन बदलें");
        change_password_label.put(ENGLISH_KEY, "Change PIN");

        forget_password_label.put(HINDI_KEY, "पिन भूल गया");
        forget_password_label.put(ENGLISH_KEY, "Forgot PIN");


        fill_change_pin_form_label.put(HINDI_KEY, "पासवर्ड बदलने के लिए नीचे दिए गए फ़ील्ड भरें");
        fill_change_pin_form_label.put(ENGLISH_KEY, "Fill the below fields to change password.");

        new_password_same_as_old_password_label.put(HINDI_KEY, "नया पिन पुराने पिन से अलग होना चाहिए।");
        new_password_same_as_old_password_label.put(ENGLISH_KEY, "New PIN should be different than old PIN.");
    }

    private void populatePhoneNumberMap() {
        phone_number_label.put(HINDI_KEY, "फ़ोन नंबर");
        phone_number_label.put(ENGLISH_KEY, "Phone Number");

        phone_number_hint.put(HINDI_KEY, "10 अंकों का फ़ोन नंबर");
        phone_number_hint.put(ENGLISH_KEY, "10 digit phone number");

        forget_password.put(ENGLISH_KEY, "Thanks for contacting Transport Network. User support team will contact you in few hours. Kindly provide your registered phone number.");
        forget_password.put(HINDI_KEY, "ट्रांसपोर्ट नेटवर्क से संपर्क करने के लिए धन्यवाद। उपयोगकर्ता सहायता टीम कुछ घंटों में आपसे संपर्क करेगी। कृपया अपना पंजीकृत फोन नंबर प्रदान करें.");

        reference_phone_number_label.put(HINDI_KEY, "संदर्भ फोन नंबर (मित्र जिसने परिवहन नेटवर्क के बारे में आपको बताया है)");
        reference_phone_number_label.put(ENGLISH_KEY, "Reference Phone number (friend who has told you about Transport Network)");
    }

    private void populateFillRegisterMap() {
        fill_required_field_label.put(HINDI_KEY, "* फ़ील्ड आवश्य भरें|");
        fill_required_field_label.put(ENGLISH_KEY, "* are required fields");
        fill_register_form_label.put(HINDI_KEY, "रजिस्टर करने के लिए नीचे फ़ील्ड भरें|");
        fill_register_form_label.put(ENGLISH_KEY, "Fill the below fields to register.");
        fill_login_form_label.put(HINDI_KEY, "लॉग इन करने के लिए नीचे फ़ील्ड भरें|");
        fill_login_form_label.put(ENGLISH_KEY, "Fill the below fields to login.");
    }

    private void populateChooseLanguageMap() {
        chose_preferred_language_label.put(HINDI_KEY, "अपनी पसंदीदा भाषा चुने,\nकिसी भी समय भाषा बदले..");
        chose_preferred_language_label.put(ENGLISH_KEY, "Select your preferred language,\ncan change language anytime..");
    }

    private void populateOpenRegisterFormMap() {
        open_register_user_activity_label.put(HINDI_KEY, "नए सदस्य, यहाँ क्लिक करें");
        open_register_user_activity_label.put(ENGLISH_KEY, "New User, Click here");

        points_rules.put(HINDI_KEY, "ट्रांसपोर्ट नेटवर्क में उपयोगकर्ता-टीएन-पॉइंट है, जो उपयोगकर्ता को परिणाम सूची पर शीर्ष पर रहने में सहायता करता है, जब अन्य उपयोगकर्ता वाहन या लोड खोजते हैं। निम्नलिखित तरीके से आप उपयोगकर्ता-टीएन-अंक कमा सकते हैं:\n\n" +
                "1. 1 अंक - जब भी मेरे वाहन में वाहन जोड़ें\n" +
                "2. 1 अंक - जब भी मेरा लोड में लोड जोड़ें\n" +
                "3. 5 अंक - पंजीकरण करते समय यदि आप \"संदर्भ फोन नंबर\" जोड़ते हैं।\n" +
                "4. 5 अंक - जब भी अन्य उपयोगकर्ता पंजीकरण के दौरान आपके नंबर का उपयोग \"संदर्भ फोन नंबर\" करते हैं|");
        points_rules.put(ENGLISH_KEY, "Transport Network has user-TN-points, which help user to be on the top in result list when other users search vehicles or loads. You can earn user-TN-points by following ways: \n\n1. 1 Point - Whenever you Add vehicle in My Vehicles.\n2. 1 Point - Whenever you Add Load in My Load.\n3. 5 Points - If you add \"Reference Phone Number\" while registration.\n4. 5 Points - Whenever other user uses your number as \"Reference Phone Number\" while registration.");
    }

    private void populateRegisterMap() {
        register_user_label.put(HINDI_KEY, "रजिस्टर");
        register_user_label.put(ENGLISH_KEY, "Register");
    }

    public static LanguageStrings getInstance() {
        if (instance == null)
            instance = new LanguageStrings();
        return instance;
    }

    public void populateStatesCitiesMap(String inputString) {
        if(inputString == null)
            return;

        TransporterSharedPreferences.getInstance().setString(STATE_CITY_KEY, inputString);
    }
}
