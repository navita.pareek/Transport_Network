package transporter.transporter_network.constants;

import transporter.transporter_network.models.User;

public final class CoreConstants {
    private final static String CoreBaseUrl = "http://transporter-network.us-east-2.elasticbeanstalk.com/";
//    private final static String CoreBaseUrl = "http://10.0.2.2:8080/";
    private final static String RegisterUserPath = "user/register";
    private final static String UpdateLanguagePath = "user/language";
    private final static String AddUserVehiclePath = "vehicle/add";
    private final static String GetConstantsForLanguagePath = "support/string_constants/";
    private final static String GetAddUserCompanyPath = "company/add";
    private final static String GetAddManpowerPath = "user/manpower/add";
    private final static String GetStatesCitiesPath = "support/states/cities/";
    private final static String GetUserVehiclePath = "user/vehicle/";
    private final static String GetUpdateVehiclePath = "vehicle/update";
    private final static String GetAddTripUrl = "trip/add";
    private final static String GetSearchVehicleForTripUrl = "trip/search_vehicle";
    private final static String GetLoginUserURL = "user/login";
    private final static String GetSearchLoadForVehiclePath = "trip/search";
    private final static String GetUserTripPath = "trip";
    private final static String GetTripUpdatePath = "trip/update";
    private final static String GetVehicleStatusUrl = "support/vehicle_status/";
    private final static String GetTripStatusUrl = "support/trip_status/";
    private final static String GetCustomerFeedbackUrl = "support/feedback";
    private final static String GetRemoveManpowerUrl = "user/manpower/remove";
    private final static String GetRemoveVehicleUrl = "vehicle/remove";
    private final static String GetUserCompanyPath = "company/";
    private static final String GetUpdateUserCompanyPath = "company/update";
    private static final String GetRemoveUserCompanyPath = "company/remove";
    private static final String GetIsUserEmpoweredPath = "company/isEmpowered/";
    private static final String GetUserCompanyMembersPath = "company/members/";
    private static final String GetIncludeUserVehiclesInCompanyPath = "company/include_vehicles";
    private static final String GetChangePasswordUrl = "user/change_password";
    private static final String GetUserTnPointsPath = "user/tn_points/";
    private static final String GetUserContactedPath = "user/user_contacted";

    public static final String SUCCESS = "TRUE";
    public static final String SERVER_UNDER_MAINTENANCE = "Server is currently under maintenance.";
    public static final String SearchEntity = "SearchEntity";
    public static final String SearchLoadForVehicle = "SearchLoadForVehicle";
    public static final String SearchVehicleForTrips = "SearchVehicleForTrips";

    public static final String phoneNumber = "phoneNumber";
    public static final String referencePhoneNumber = "referencePhoneNumber";
    public static final String name = "name";
    public static final String dateOfBirth = "dateOfBirth";
    public static final String password = "password";
    public static final String language = "language";
    public final static String users = "users";
    public static final String oldPassword = "oldPassword";
    public static final String userTnPoints = "userTnPoints";

    public static final String vehicleNumber = "vehicleNumber";
    public static final String capacity = "capacity";
    public static final String currentState = "currentState";
    public static final String currentCity = "currentCity";
    public static final String status = "status";
    public static final String isLinkedToCompany = "isLinkedToCompany";
    public final static String vehicles = "vehicles";

    public static final String companyName = "companyName";

    public static final String newManpowerPhoneNumber = "newManpowerPhoneNumber";
    public static final String isEmpowerForStructureChange = "isEmpowerForStructureChange";

    public static final String startDate = "startDate";
    public static final String startState = "startState";
    public static final String startCity = "startCity";
    public static final String endState = "endState";
    public static final String endCity = "endCity";
    public static final String vehicleCapacityRequired = "vehicleCapacityRequired";
    public static final String newStatus = "newStatus";
    public final static String trips = "trips";
    public static final String visible = "visible";

    public static final String isHappy = "isHappy";
    public static final String wantToConnect = "wantToConnect";

    public final static String USER_REGISTER_ID = "USER_REGISTER_ID";
    public final static String SearchVehicleResult = "SearchVehicleResult";
    public static final String USER_COMPANY_NAME = "USER_COMPANY_NAME";
    public static final String USER_IS_EMPOWERED = "USER_IS_EMPOWERED";

    public static String getRegisterUserUrl(){
        return CoreBaseUrl + RegisterUserPath;
    }

    public static String getAddUserVehicleUrl() {
        return CoreBaseUrl + AddUserVehiclePath;
    }

    public static String getStringConstantsUrl() {
        return CoreBaseUrl + GetConstantsForLanguagePath;
    }

    public static String getAddUserCompanyUrl() {
        return CoreBaseUrl + GetAddUserCompanyPath;
    }

    public static String getAddManpowerUrl() {
        return CoreBaseUrl + GetAddManpowerPath;
    }

    public static String getStatesCitiesUrl(String preferredLanguage) {
        return CoreBaseUrl + GetStatesCitiesPath + preferredLanguage;
    }

    public static String getUserVehicleUrl() {
        return CoreBaseUrl + GetUserVehiclePath;
    }

    public static String getUpdateVehicleUrl() {
        return CoreBaseUrl + GetUpdateVehiclePath;
    }

    public static String getAddTripUrl() {
        return CoreBaseUrl + GetAddTripUrl;
    }

    public static String getLoginUserUrl() {
        return CoreBaseUrl + GetLoginUserURL;
    }

    public static String getSearchLoadForVehicleUrl() {
        return CoreBaseUrl + GetSearchLoadForVehiclePath;
    }

    public static String getUserTripsUrl(User user) {
        return CoreBaseUrl + GetUserTripPath + "/" + user.getPhoneNumber();
    }

    public static String getUpdateTripUrl() {
        return CoreBaseUrl + GetTripUpdatePath;
    }

    public static String getVehicleStatusUrl(String phoneNumber) {
        return CoreBaseUrl + GetVehicleStatusUrl + phoneNumber;
    }

    public static String getTripStatusUrl(String phoneNumber) {
        return CoreBaseUrl + GetTripStatusUrl + phoneNumber;
    }

    public static String getUpdateUserLanguageUrl() {
        return CoreBaseUrl + UpdateLanguagePath;
    }

    public static String getSaveCustomerFeedBackUrl() {
        return CoreBaseUrl + GetCustomerFeedbackUrl;
    }

    public static String getRemoveManpowerUrl() {
        return CoreBaseUrl + GetRemoveManpowerUrl;
    }

    public static String getRemoveVehicleUrl() {
        return CoreBaseUrl + GetRemoveVehicleUrl;
    }

    public static String getUserCompanyUrl(String phoneNumber) {
        return CoreBaseUrl + GetUserCompanyPath + phoneNumber;
    }

    public static String getUpdateUserCompanyUrl() {
        return CoreBaseUrl + GetUpdateUserCompanyPath;
    }

    public static String getRemoveUserCompanyUrl() {
        return CoreBaseUrl + GetRemoveUserCompanyPath;
    }

    public static String getIsUserEmpoweredUrl(String phoneNumber) {
        return CoreBaseUrl + GetIsUserEmpoweredPath + phoneNumber;
    }

    public static String getUserCompanyMembersUrl(String phoneNumber) {
        return CoreBaseUrl + GetUserCompanyMembersPath + phoneNumber;
    }

    public static String getIncludeUserVehiclesInCompanyUrl() {
        return CoreBaseUrl + GetIncludeUserVehiclesInCompanyPath;
    }

    public static String getSearchVehicleForTripUrl() {
        return CoreBaseUrl + GetSearchVehicleForTripUrl;
    }

    public static String getChangePasswordUrl() {
        return CoreBaseUrl + GetChangePasswordUrl;
    }

    public static String getUserTnPointsUrl(String phoneNumber) {
        return CoreBaseUrl + GetUserTnPointsPath + phoneNumber;
    }

    public static String getUserContactedUrl() {
        return CoreBaseUrl + GetUserContactedPath;
    }
}
