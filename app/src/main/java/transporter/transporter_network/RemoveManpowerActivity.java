package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.Manpower;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserManpower;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class RemoveManpowerActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_manpower);

        getPreferredLanguage();
        setTitleText();
        setMemberPhoneNumberLabel();
        setSubmitButtonLabel();
    }

    private void setTitleText() {
        TextView text = (TextView) findViewById(R.id.TextView_fill_remove_member_form_label);
        text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_remove_member_form_label));
        text.setTextColor(Color.BLACK);
    }

    private void setMemberPhoneNumberLabel(){
        TextView text = (TextView) findViewById(R.id.textView_to_remove_number_label);
        text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().phone_number_to_remove));

    }
    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void setSubmitButtonLabel() {
        Button button = (Button) findViewById(R.id.button_remove_manpower);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().remove_manpower_label));
    }

    public void OnRemoveManpowerClicked(View view){
        setTitleText();
        if(isRequiredFieldProvided() && isRequiredInputValid()){
            AlertDialog.Builder builder = new AlertDialog.Builder(RemoveManpowerActivity.this);
            builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().remove_company_member_confirmation_msg));
            builder.setCancelable(false);
            builder.setNegativeButton(
                    TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder.setPositiveButton(
                    TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            try {
                                RemoveManpower();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private boolean isRequiredInputValid() {
        String toRemovePhoneNumber = ((TextView) findViewById(R.id.editText_to_remove_member_phone_number)).getText().toString().trim();
        if(toRemovePhoneNumber.length() != 10 || toRemovePhoneNumber.equals(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID))){
            TextView textView = (TextView)findViewById(R.id.TextView_fill_remove_member_form_label);
            textView.setText(LanguageStrings.getInstance().invalid_phone_number_label.get(preferredLanguage));
            textView.setTextColor(Color.RED);
            return false;
        }

        return true;
    }

    private boolean isRequiredFieldProvided() {
        if (((TextView) findViewById(R.id.editText_to_remove_member_phone_number)).getText().toString().trim().length() > 0){
            return true;
        }

        TextView textView = (TextView)findViewById(R.id.TextView_fill_remove_member_form_label);
        textView.setText(LanguageStrings.getInstance().fill_required_field_label.get(preferredLanguage));
        textView.setTextColor(Color.RED);
        return false;
    }

    private void RemoveManpower() {
        String newMemberPhoneNumber = ((EditText) findViewById(R.id.editText_to_remove_member_phone_number)).getText().toString();
        Manpower manpower = new Manpower(newMemberPhoneNumber);

        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        UserManpower userManpower = new UserManpower(user, manpower);

        (new RemoveManpowerActivity.removeManpower(RemoveManpowerActivity.this)).execute(userManpower);
    }

    private class removeManpower extends AsyncTask<UserManpower, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private removeManpower(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserManpower... userManpower) {
            String result = null;
            for(int index = 0; index < userManpower.length; index++){
                result = CoreAdapter.removeManpower(userManpower[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("TRUE")){
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
                ((EditText) findViewById(R.id.editText_to_remove_member_phone_number)).setText("");
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
