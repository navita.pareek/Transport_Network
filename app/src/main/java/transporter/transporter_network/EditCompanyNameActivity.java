package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.Company;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserCompany;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class EditCompanyNameActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_company_name);

        getPreferredLanguage();
        setCompanyNameTextHint();
        setEditCompanyName();
        setEditCompanyButtonText();
    }

    private void setEditCompanyButtonText() {
        Button button = (Button) findViewById(R.id.button_update_company);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().update_company_name_label));
    }

    private void setEditCompanyName(){
        TextView companyName = (TextView) findViewById(R.id.textView_new_company_name_label);
        companyName.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().edit_company_name_label));
        companyName.setTextColor(Color.BLACK);
    }

    public void OnUpdateCompanySubmitClicked(View view){
        setEditCompanyName();
        try {
            if(differentNewName()) {
                updateCompany();
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
        }
    }

    private boolean differentNewName() {
        String currentName = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_COMPANY_NAME);
        String newName = ((EditText) findViewById(R.id.editText_company_name)).getText().toString();

        if(currentName.trim().equals(newName.trim())){
            TextView companyName = (TextView) findViewById(R.id.textView_new_company_name_label);
            companyName.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().same_edit_company_name));
            companyName.setTextColor(Color.RED);
            return false;
        }

        return true;
    }


    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void setCompanyNameTextHint() {
        EditText editText = (EditText) findViewById(R.id.editText_company_name);
        editText.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_COMPANY_NAME));
        editText.setSelection(editText.getText().length());
    }

    private void updateCompany() throws Exception {
        String companyName = ((EditText) findViewById(R.id.editText_company_name)).getText().toString();

        Company company = new Company(companyName);

        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        UserCompany userCompany = new UserCompany(user, company);

        (new EditCompanyNameActivity.updateCompany(EditCompanyNameActivity.this)).execute(userCompany);
    }


    private class updateCompany extends AsyncTask<UserCompany, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private updateCompany(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserCompany... userCompany) {
            String result = null;
            for(int index = 0; index < userCompany.length; index++){
                result = CoreAdapter.updateUserCompany(userCompany[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("TRUE")){
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
                TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_COMPANY_NAME,
                        ((EditText) findViewById(R.id.editText_company_name)).getText().toString());
                ((EditText) findViewById(R.id.editText_company_name)).setText("");
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else if(result.contains(LanguageStrings.getInstance().BAD_REQUEST)){
                    builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().bad_request_toast));
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

}
