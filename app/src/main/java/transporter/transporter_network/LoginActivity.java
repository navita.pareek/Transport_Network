package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.CustomerFeedback;
import transporter.transporter_network.models.User;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class LoginActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getPreferredLanguage();
        updateRegisterFormLabelText();
        updatePhoneNumberLabelHint();
        updatePasswordLabel();
        updateRegisterButtonText();
    }

    public void onChangePasswordClick(View view){
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), ChangePasswordActivity.class);
        startActivity(intent);
    }

    public void onForgetPasswordClick(View view){
        View editTextView = View.inflate(this, R.layout.edit_text, null);
        final EditText textBox = (EditText) editTextView.findViewById(R.id.editText);
        textBox.setHint(LanguageStrings.getInstance().phone_number_hint.get(preferredLanguage));

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage(LanguageStrings.getInstance().forget_password.get(preferredLanguage));
        builder.setView(editTextView);
        builder.setCancelable(true);
        builder.setPositiveButton(
                LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        String phoneNumber = textBox.getText().toString();
                        CustomerFeedback customerFeedback = new CustomerFeedback(phoneNumber, LanguageStrings.getInstance().FORGET_PASSWORD);
                        customerFeedback.setWantToConnect(true);
                        (new LoginActivity.updateCustomerFeedback(LoginActivity.this)).execute(customerFeedback);
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void updateRegisterFormLabelText() {
        TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
        registerMeButton.setText(LanguageStrings.getInstance().fill_login_form_label.get(preferredLanguage));
        registerMeButton.setTextColor(Color.BLACK);
    }

    public void OnLoginClick(View view){
        updateRegisterFormLabelText();

        if(areRequiredFieldsFilled() && areRequiredFieldsValid()){
            String phoneNumber = ((EditText) findViewById(R.id.editText_phone_number)).getText().toString();
            String password = ((EditText) findViewById(R.id.editText_passcode)).getText().toString();
            User user = new User(phoneNumber, "", password);
            user.setLanguage(preferredLanguage);
            (new LoginActivity.loginUser(LoginActivity.this)).execute(user);
        }
    }

    private boolean areRequiredFieldsValid() {
        if(isInvalidPhoneNumber()){
            TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
            registerMeButton.setText(LanguageStrings.getInstance().invalid_phone_number_label.get(preferredLanguage));
            registerMeButton.setTextColor(Color.RED);
            return false;
        }

        if(isInValidPassword()){
            TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
            registerMeButton.setText(LanguageStrings.getInstance().invalid_password_label.get(preferredLanguage));
            registerMeButton.setTextColor(Color.RED);
            return false;
        }

        return true;
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }


    private void updatePhoneNumberLabelHint() {
        EditText registerMeButton = (EditText) findViewById(R.id.editText_phone_number);
        registerMeButton.setHint(LanguageStrings.getInstance().phone_number_hint.get(preferredLanguage));

        TextView registerMeButton1 = (TextView) findViewById(R.id.textView_phone_number_label);
        registerMeButton1.setText(LanguageStrings.getInstance().phone_number_label.get(preferredLanguage));
    }

    private void updatePasswordLabel() {
        TextView registerMeButton1 = (TextView) findViewById(R.id.textView_password_label);
        registerMeButton1.setText(LanguageStrings.getInstance().password_label.get(preferredLanguage));

        registerMeButton1 = (TextView) findViewById(R.id.editText_forget_password);
        registerMeButton1.setText(LanguageStrings.getInstance().forget_password_label.get(preferredLanguage));

        registerMeButton1 = (TextView) findViewById(R.id.editText_change_password);
        registerMeButton1.setText(LanguageStrings.getInstance().change_password_label.get(preferredLanguage));
    }

    private void updateRegisterButtonText() {
        Button registerMeButton = (Button) findViewById(R.id.button_login);
        registerMeButton.setText(LanguageStrings.getInstance().login_button_label.get(preferredLanguage));
    }

    public boolean areRequiredFieldsFilled() {
        if (((EditText) findViewById(R.id.editText_phone_number)).getText().toString().trim().length() == 0 ||
                ((EditText) findViewById(R.id.editText_passcode)).getText().toString().trim().length() == 0 ){
            TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
            registerMeButton.setText(LanguageStrings.getInstance().fill_required_field_label.get(preferredLanguage));
            registerMeButton.setTextColor(Color.RED);
            return false;
        }
        return true;
    }

    public boolean isInvalidPhoneNumber() {
        return ((EditText) findViewById(R.id.editText_phone_number)).getText().toString().trim().length() != 10;
    }

    public boolean isInValidPassword() {
        return ((EditText) findViewById(R.id.editText_passcode)).getText().toString().trim().length() != 6;
    }

    private class loginUser extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;
        private String userPhoneNumber;

        private loginUser(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.loginUser(user[index]);
                this.userPhoneNumber = user[index].getPhoneNumber();
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                Toast.makeText(activity, LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_SHORT).show();
                SeedData.getInstance().populateUserId(userPhoneNumber);
                SeedData.getInstance().populateUserLanguage(result);
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), WelcomeActivity.class);
                startActivity(intent);
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null  &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class updateCustomerFeedback extends AsyncTask<CustomerFeedback, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private updateCustomerFeedback(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(CustomerFeedback... params) {
            String result = null;
            for(int i = 0; i < params.length; i++)
                result = CoreAdapter.addCustomerFeedback(params[i]);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                Toast.makeText(activity, LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}


