package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.Company;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserCompany;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class AddCompanyNameActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_company_name);

        getPreferredLanguage();
        setFillFormLabel();
        setCompanyNameTextHint();
        setAddCompanyButtonText();
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void setAddCompanyButtonText() {
        Button button = (Button) findViewById(R.id.button_add_company);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().add_company_name_label));
    }

    private void setCompanyNameTextHint() {
        TextView text = (TextView) findViewById(R.id.textView_company_label);
        text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().company_name_label));
    }

    private void setFillFormLabel() {
        TextView text = (TextView) findViewById(R.id.TextView_fill_add_company_form_label);
        text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_company_name_label));
        text.setTextColor(Color.BLACK);
    }

    public void OnAddCompanySubmitClicked(View view){
        setFillFormLabel();

        if(allUserInputsAreValid()){
            try {
                addCompany();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void addCompany() throws Exception {
        String companyName = ((EditText) findViewById(R.id.editText_company_name)).getText().toString();

        Company company = new Company(companyName);

        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        UserCompany userCompany = new UserCompany(user, company);

        (new AddCompanyNameActivity.addCompany(AddCompanyNameActivity.this)).execute(userCompany);
    }

    private void includeUserVehiclesInCompany(){
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new AddCompanyNameActivity.includeUserVehiclesInCompany(AddCompanyNameActivity.this)).execute(user);
    }

    private boolean allUserInputsAreValid() {
        TextView textView = (TextView)findViewById(R.id.TextView_fill_add_company_form_label);

        if(!areAllRequiredFieldsFilled()){
            textView.setText(LanguageStrings.getInstance().fill_required_field_label.get(preferredLanguage));
            textView.setTextColor(Color.RED);
            return false;
        }

        return true;
    }

    private boolean areAllRequiredFieldsFilled() {
        return (((EditText) findViewById(R.id.editText_company_name)).getText().toString().trim().length() != 0);
    }

    private class addCompany extends AsyncTask<UserCompany, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private addCompany(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserCompany... userCompany) {
            String result = null;
            for(int index = 0; index < userCompany.length; index++){
                result = CoreAdapter.addUserCompany(userCompany[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("TRUE")){
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
                TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_COMPANY_NAME,((EditText) findViewById(R.id.editText_company_name)).getText().toString());
                includeUserVehiclesInCompany();
                Intent intent = new Intent(AddCompanyNameActivity.this, DashboardActivity.class);
                startActivity(intent);
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else if(result.contains(LanguageStrings.getInstance().BAD_REQUEST)){
                    builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().bad_request_toast));
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class includeUserVehiclesInCompany extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private includeUserVehiclesInCompany(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... users) {
            String result = null;
            for(int index = 0; index < users.length; index++){
                result = CoreAdapter.includeUserVehiclesInCompany(users[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // DO NOTHING
        }
    }
}
