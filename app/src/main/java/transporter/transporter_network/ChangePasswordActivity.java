package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.User;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class ChangePasswordActivity extends AppCompatActivity {

    private String preferredLanguage;

    private final int MIN_AGE = 15;
    private final int MAX_AGE = 80;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        getPreferredLanguage();
        updateRegisterFormLabelText();
        updatePhoneNumberLabel();
        updatePhoneNumberHint();
        updateOldPasswordLabel();
        updatePasswordLabel();
        updateRegisterButtonText();
    }

    private void updateOldPasswordLabel() {
        TextView registerMeButton = (TextView) findViewById(R.id.textView_old_password_label);
        registerMeButton.setText(LanguageStrings.getInstance().old_password_label.get(preferredLanguage));
    }

    private void updateRegisterButtonText() {
        Button registerMeButton = (Button) findViewById(R.id.submit_change_password);
        registerMeButton.setText(LanguageStrings.getInstance().change_password_label.get(preferredLanguage));
    }

    private void updatePasswordLabel() {
        TextView registerMeButton = (TextView) findViewById(R.id.textView_password_label);
        registerMeButton.setText(LanguageStrings.getInstance().password_label.get(preferredLanguage));

        registerMeButton = (TextView) findViewById(R.id.textView_confirm_password_label);
        registerMeButton.setText(LanguageStrings.getInstance().confirm_password_label.get(preferredLanguage));
    }

    private void updatePhoneNumberLabel() {
        TextView registerMeButton = (TextView) findViewById(R.id.textView_phone_number_label);
        registerMeButton.setText(LanguageStrings.getInstance().phone_number_label.get(preferredLanguage));
    }

    private void updatePhoneNumberHint() {
        EditText registerMeButton = (EditText) findViewById(R.id.editText_phone_number);
        registerMeButton.setHint(LanguageStrings.getInstance().phone_number_hint.get(preferredLanguage));
    }

    private void updateRegisterFormLabelText() {
        TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
        registerMeButton.setText(LanguageStrings.getInstance().fill_change_pin_form_label.get(preferredLanguage));
        registerMeButton.setTextColor(Color.BLACK);
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private boolean allUserInputsAreValid() {
        TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);

        if(!areAllFieldsFilled()){
            TextView title = (TextView)findViewById(R.id.TextView_search_result_label);
            title.setText(LanguageStrings.getInstance().fill_required_field_label.get(preferredLanguage));
            title.setTextColor(Color.RED);
            return false;
        }

        if(!isValidPhoneNumber()){
            registerMeButton.setText(LanguageStrings.getInstance().invalid_phone_number_label.get(preferredLanguage));
            registerMeButton.setTextColor(Color.RED);
            return false;
        }

        if(!invalid_password_label()){
            registerMeButton.setText(LanguageStrings.getInstance().invalid_password_confirm_password_label.get(preferredLanguage));
            registerMeButton.setTextColor(Color.RED);
            return false;
        }

        if(newAndOldPasswordSame()){
            registerMeButton.setText(LanguageStrings.getInstance().new_password_same_as_old_password_label.get(preferredLanguage));
            registerMeButton.setTextColor(Color.RED);
            return false;
        }

        return true;
    }

    private boolean newAndOldPasswordSame() {
        EditText oldPasswordText = (EditText) findViewById(R.id.editText_old_password);
        EditText passwordText = (EditText) findViewById(R.id.editText_password);

        return passwordText.getText().toString().equals(oldPasswordText.getText().toString());
    }

    public void OnChangePinButtonClick(View view) {
        updateRegisterFormLabelText();
        if(allUserInputsAreValid()){
            try {
                changePassword();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean areAllFieldsFilled() {
        return (((EditText) findViewById(R.id.editText_phone_number)).getText().toString().trim().length() != 0 &&
                ((EditText) findViewById(R.id.editText_old_password)).getText().toString().trim().length() != 0 &&
                ((EditText) findViewById(R.id.editText_password)).getText().toString().trim().length() != 0 &&
                ((EditText) findViewById(R.id.editText_confirm_password)).getText().toString().trim().length() != 0);
    }

    private boolean isValidPhoneNumber() {
        EditText phoneNumber = (EditText) findViewById(R.id.editText_phone_number);
        return phoneNumber.getText().toString().trim().length() == 10;
    }

    private boolean invalid_password_label() {

        EditText oldPasswordText = (EditText) findViewById(R.id.editText_old_password);
        EditText confirmPasswordText = (EditText) findViewById(R.id.editText_confirm_password);
        EditText passwordText = (EditText) findViewById(R.id.editText_password);

        if(!passwordText.getText().toString().equals(confirmPasswordText.getText().toString()))
            return false;

        return passwordText.getText().toString().trim().length() == 6 && oldPasswordText.getText().toString().trim().length() == 6;
    }

    private void changePassword() throws Exception {
        String phoneNumber = ((EditText) findViewById(R.id.editText_phone_number)).getText().toString();
        String dateOfBirth = "09/09/1990";
        String password = ((EditText) findViewById(R.id.editText_password)).getText().toString();
        String oldPassword = ((EditText) findViewById(R.id.editText_old_password)).getText().toString();

        User user = new User(phoneNumber, dateOfBirth, password);
        user.setOldPassword(oldPassword);
        user.setLanguage(preferredLanguage);

        (new changePassword(ChangePasswordActivity.this)).execute(user);
    }

    private class changePassword extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;
        private String userPhoneNumber;

        private changePassword(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.changePassword(user[index]);
                this.userPhoneNumber = user[index].getPhoneNumber();
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.equals(CoreConstants.SUCCESS)){
                Toast.makeText(activity, LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_SHORT).show();
                SeedData.getInstance().populateUserId(userPhoneNumber);
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), WelcomeActivity.class);
                startActivity(intent);
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
