package transporter.transporter_network.tools;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.models.CustomerFeedback;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserCompany;
import transporter.transporter_network.models.UserManpower;
import transporter.transporter_network.models.UserTrip;
import transporter.transporter_network.models.UserVehicle;
import transporter.transporter_network.models.Vehicle;

public final class CoreAdapter {

    public static String registerUser(User user){
        String url = CoreConstants.getRegisterUserUrl();
        String body = user.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }


    public static String addUserVehicle(UserVehicle userVehicle) {
        String url = CoreConstants.getAddUserVehicleUrl();
        String body = userVehicle.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String getStringConstants(String language) {
        String url = CoreConstants.getStringConstantsUrl() + language;
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String addUserCompany(UserCompany userCompany) {
        String url = CoreConstants.getAddUserCompanyUrl();
        String body = userCompany.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String addManpower(UserManpower userManpower) {
        String url = CoreConstants.getAddManpowerUrl();
        String body = userManpower.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String getStatesCities(String preferredLanguage) {
        String url = CoreConstants.getStatesCitiesUrl(preferredLanguage);
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String getUserVehicle(User user) {
        String url = CoreConstants.getUserVehicleUrl() + user.getPhoneNumber();
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String updateVehicle(Vehicle vehicle) {
        String url = CoreConstants.getUpdateVehicleUrl();
        String body = vehicle.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String addTrip(UserTrip userTrip) {
        String url = CoreConstants.getAddTripUrl();
        String body = userTrip.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String searchVehicleForTrip(UserTrip userTrip) {
        String url = CoreConstants.getSearchVehicleForTripUrl();
        String body = userTrip.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String loginUser(User user) {
        String url = CoreConstants.getLoginUserUrl();
        String body = user.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String searchLoadForVehicle(UserTrip userTrip) {
        String url = CoreConstants.getSearchLoadForVehicleUrl();
        String body = userTrip.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String getUserTrips(User user) {
        String url = CoreConstants.getUserTripsUrl(user);
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String updateTrip(UserTrip userTrip) {
        String url = CoreConstants.getUpdateTripUrl();
        String body = userTrip.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String getVehicleStatus(String phoneNumber) {
        String url = CoreConstants.getVehicleStatusUrl(phoneNumber);
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String getTripStatus(String phoneNumber) {
        String url = CoreConstants.getTripStatusUrl(phoneNumber);
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String updateUserLanguage(User user) {
        String url = CoreConstants.getUpdateUserLanguageUrl();
        String body = user.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String addCustomerFeedback(CustomerFeedback param) {
        String url = CoreConstants.getSaveCustomerFeedBackUrl();
        String body = param.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String removeManpower(UserManpower userManpower) {
        String url = CoreConstants.getRemoveManpowerUrl();
        String body = userManpower.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String removeVehicle(UserVehicle userVehicle) {
        String url = CoreConstants.getRemoveVehicleUrl();
        String body = userVehicle.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String getUserCompany(String phoneNumber) {
        String url = CoreConstants.getUserCompanyUrl(phoneNumber);
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String updateUserCompany(UserCompany userCompany) {
        String url = CoreConstants.getUpdateUserCompanyUrl();
        String body = userCompany.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String removeUserCompany(User user) {
        String url = CoreConstants.getRemoveUserCompanyUrl();
        String body = user.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String getIsUserEmpowered(String phoneNumber) {
        String url = CoreConstants.getIsUserEmpoweredUrl(phoneNumber);
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String getUserCompanyMembers(User user) {
        String url = CoreConstants.getUserCompanyMembersUrl(user.getPhoneNumber());
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String includeUserVehiclesInCompany(User user) {
        String url = CoreConstants.getIncludeUserVehiclesInCompanyUrl();
        String body = user.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String changePassword(User user) {
        String url = CoreConstants.getChangePasswordUrl();
        String body = user.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }

    public static String getUserTnPoints(String phoneNumber) {
        String url = CoreConstants.getUserTnPointsUrl(phoneNumber);
        return (new CoreClient()).SimpleGetConnection(url);
    }

    public static String userContacted(User user) {
        String url = CoreConstants.getUserContactedUrl();
        String body = user.toSerializedMap();
        return (new CoreClient()).SimplePostConnection(url, body);
    }
}
