package transporter.transporter_network.tools;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.Trip;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.Vehicle;

public class SeedData {

    public Map<String, List<String>> stateCity = new LinkedHashMap<>();
    public List<String> companyMembers = new ArrayList<>();
    public List<Vehicle> vehicles = new ArrayList<>();
    public List<Trip> trips = new ArrayList<>();
    public List<String> vehicleStatus = new ArrayList<>();
    public List<String> tripStatus = new ArrayList<>();

    private static SeedData ourInstance = null;

    public static SeedData getInstance() {
        if(ourInstance == null)
            ourInstance = new SeedData();
        return ourInstance;
    }

    private SeedData() {
    }

    public void populateStateCityMap() {
        String stateCityString = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().STATE_CITY_KEY);

        if(stateCityString == null)
            return;

        stateCity.clear();

        String[] interimArray = stateCityString.split(",");
        for(int index = 0; index < interimArray.length; index++){
            if(interimArray[index].trim().length() > 0){
                String[] temp = interimArray[index].split(":");
                if(temp[1].equals("null")) {
                    stateCity.put(temp[0], new ArrayList<String>());
                }else{
                    stateCity.get(temp[0]).add(temp[1]);
                }
            }
        }
    }

    public void populateVehiclesMap() {
        String vehiclesString = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().VEHICLES_KEY);

        if(vehiclesString == null)
            return;

        vehicles.clear();
        try {
            JSONObject mainJsonObject = new JSONObject(vehiclesString);
            JSONArray interimArray = mainJsonObject.getJSONArray(CoreConstants.vehicles);
            for (int index = 0; index < interimArray.length(); index++) {
                JSONObject jsonObject = interimArray.getJSONObject(index);
                String vehicleNumber = jsonObject.getString("vehicleNumber");
                String capacity = jsonObject.getString("capacity");
                String startState = getStateByLocation(jsonObject.getString("startLocation"));
                String startCity = getCityByLocation(jsonObject.getString("startLocation"));
                String endState = getStateByLocation(jsonObject.getString("endLocation"));
                String endCity = getCityByLocation(jsonObject.getString("endLocation"));
                String status = getVehicleStatusByIndex(jsonObject.getString("status"));

                Vehicle vehicle = new Vehicle(vehicleNumber, capacity);
                vehicle.setStartState(getStateIndex(jsonObject.getString("startLocation")));
                vehicle.setStartCity(getCityIndex(jsonObject.getString("startLocation")));
                vehicle.setEndState(getStateIndex(jsonObject.getString("endLocation")));
                vehicle.setEndCity(getCityIndex(jsonObject.getString("endLocation")));
                vehicle.setStatus(jsonObject.getString("status"));

                vehicle.setStartStateName(startState);
                vehicle.setStartCityName(startCity);
                vehicle.setEndStateName(endState);
                vehicle.setEndCityName(endCity);
                vehicle.setStatusName(status);

                vehicles.add(vehicle);
            }
        }catch (Exception ex){
            System.out.println(ex);
        }
    }

    private String getVehicleStatusByIndex(String index) {
        if(Integer.parseInt(index) < 1 || Integer.parseInt(index) > vehicleStatus.size())
            return "";

        return vehicleStatus.get(Integer.parseInt(index) - 1);
    }

    public void populateUserId(String userPhoneNumber){
        TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_REGISTER_ID, userPhoneNumber);
    }

    public void populateTripsMap() {
        String tripsString = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().TRIPS_KEY);

        if(tripsString == null)
            return;

        trips.clear();
        try {
            JSONObject mainJsonObject = new JSONObject(tripsString);
            JSONArray interimArray = mainJsonObject.getJSONArray(CoreConstants.trips);
            for (int index = 0; index < interimArray.length(); index++) {
                JSONObject jsonObject = interimArray.getJSONObject(index);
                String startDate = jsonObject.getString("startDate");
                String startState = getStateByLocation(jsonObject.getString("startLocation"));
                String startCity = getCityByLocation(jsonObject.getString("startLocation"));
                String endState = getStateByLocation(jsonObject.getString("endLocation"));
                String endCity = getCityByLocation(jsonObject.getString("endLocation"));
                String vehicleCapacityRequired = jsonObject.getString("vehicleCapacityRequired");
                String status = getTripStatusByIndex(jsonObject.getString("status"));

                Trip trip = new Trip();
                trip.setStartDate(startDate);
                trip.setStartState(getStateIndex(jsonObject.getString("startLocation")));
                trip.setStartCity(getCityIndex(jsonObject.getString("startLocation")));
                trip.setEndState(getStateIndex(jsonObject.getString("endLocation")));
                trip.setEndCity(getCityIndex(jsonObject.getString("endLocation")));
                trip.setStatus(jsonObject.getString("status"));

                trip.setStartStateName(startState);
                trip.setStartCityName(startCity);
                trip.setEndStateName(endState);
                trip.setEndCityName(endCity);
                trip.setPerVehicleCapacity(vehicleCapacityRequired);
                trip.setStatusName(status);

                trips.add(trip);
            }
        }catch (Exception ex){
            System.out.println(ex);
        }
    }

    private String getCityIndex(String location) {
        return location.substring(location.indexOf(':') + 1);
    }

    private String getStateIndex(String location) {
        return location.substring(0, location.indexOf(':'));
    }

    private String getTripStatusByIndex(String index) {
        if(Integer.parseInt(index) < 1 || Integer.parseInt(index) > tripStatus.size())
            return "";

        return tripStatus.get(Integer.parseInt(index) - 1);
    }

    public void populateVehicleStatusList(String result) {
        TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().VEHICLE_STATUS_KEY, result);

        if(result == null)
            return;

        vehicleStatus.clear();
        String[] interimArray = result.split(",");
        for(int index = 0; index < interimArray.length; index++){
            if(interimArray[index].trim().length() > 0)
                vehicleStatus.add(interimArray[index]);
        }
    }

    public void populateTripStatusList(String result) {
        TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().TRIP_STATUS_KEY, result);

        if(result == null)
            return;

        tripStatus.clear();
        String[] interimArray = result.split(",");
        for(int index = 0; index < interimArray.length; index++){
            if(interimArray[index].trim().length() > 0)
                tripStatus.add(interimArray[index]);
        }
    }

    public void populateUserLanguage(String result) {
        if(result == null)
            return;
        try {
            JSONObject jsonObject = new JSONObject(result);
            String language = jsonObject.getString("language");
            TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY, language);
        }catch (Exception ex){
            System.out.println(ex);
        }
    }

    private String getStateByLocation(String location) {
        int stateIndex = Integer.parseInt(location.substring(0, location.indexOf(':'))) - 1;
        if(stateIndex > -1){
            return getStateByIndex(stateIndex);
        }
        return "";
    }

    private String getCityByLocation(String location) {
        int stateIndex = Integer.parseInt(location.substring(0, location.indexOf(':'))) - 1;
        int cityIndex = Integer.parseInt(location.substring(location.indexOf(':') + 1)) - 1;
        if(stateIndex > -1){
            String state = getStateByIndex(stateIndex);
            if(cityIndex > - 1 && state.length() > 0){
                return stateCity.get(state).get(cityIndex);
            }
        }
        return "";
    }

    private String getStateByIndex(int index) {
        int i = 0;
        for(Map.Entry<String, List<String>> stateCityEntry : stateCity.entrySet()){
            if(i == index){
                return stateCityEntry.getKey();
            }
            i++;
        }
        return "";
    }

    public void populateCompanyMembers() {
        String companyMembersString = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().COMPANY_MEMBERS_KEY);

        if(companyMembersString == null)
            return;

        companyMembers.clear();
        try {
            JSONObject mainJsonObject = new JSONObject(companyMembersString);
            JSONArray interimArray = mainJsonObject.getJSONArray(CoreConstants.users);
            for (int index = 0; index < interimArray.length(); index++) {
                JSONObject jsonObject = interimArray.getJSONObject(index);
                String phoneNumber = jsonObject.getString("phoneNumber");

                if(!companyMembers.contains(phoneNumber)){
                    companyMembers.add(phoneNumber);
                }
            }
        }catch (Exception ex){
            System.out.println(ex);
        }
    }
}
