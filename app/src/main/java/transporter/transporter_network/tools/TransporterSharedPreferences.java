package transporter.transporter_network.tools;

import android.content.SharedPreferences;

public class TransporterSharedPreferences {

    private static TransporterSharedPreferences instance = null;

    private static SharedPreferences transportSharedPreference;

    private TransporterSharedPreferences(){}

    public static TransporterSharedPreferences getInstance(){
        if(instance == null)
            instance = new TransporterSharedPreferences();
        return instance;
    }

    public void setSharedPreference(SharedPreferences sharedPreference){
        transportSharedPreference = sharedPreference;
    }

    public void setString(String key, String value){
        android.content.SharedPreferences.Editor editor = transportSharedPreference.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreferenceValue(String key){
        return transportSharedPreference.getString(key, null);
    }
}
