package transporter.transporter_network.tools;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CoreClient {

    private HttpURLConnection urlConnection;

    private HttpURLConnection getHttpUrlConnection(String url, String method, boolean isOutputPresent) throws Exception{
        urlConnection = (HttpURLConnection) (new URL(url)).openConnection();
        urlConnection.setRequestMethod(method);
        urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        urlConnection.setDoOutput(isOutputPresent);
        return urlConnection;
    }

    public String SimplePostConnection(String url, String stringToWrite){
        StringBuffer result = null;
        try {
            getHttpUrlConnection(url, "POST", true);
            writeToConnection(stringToWrite);
            result = readFromConnection();
        } catch (Exception e) {
            return "NOT_CONNECTED_TO_INTERNET";
        }

        if(null != result)
            return result.toString();
        return null;
    }

    public String SimpleGetConnection(String url){
        StringBuffer result = null;
        try {
            getHttpUrlConnection(url, "GET", false);
            result = readFromConnection();
        }  catch (Exception e) {
            return "NOT_CONNECTED_TO_INTERNET";
        }

        if(null != result)
            return result.toString();
        return null;
    }

    private StringBuffer readFromConnection() throws IOException {
        StringBuffer result = new StringBuffer();
        InputStream inputStream = null;

        int statusCode = urlConnection.getResponseCode();
        if (statusCode != 200) {
            inputStream = urlConnection.getErrorStream();
        }else{
            inputStream = urlConnection.getInputStream();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String input;
        while ((input = reader.readLine()) != null) {
            result.append(input);
        }
        reader.close();
        return result;
    }

    private void writeToConnection(String stringToWrite) throws Exception{
        DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
        dStream.writeBytes(stringToWrite);
        dStream.flush();
        dStream.close();
    }
}