package transporter.transporter_network.models;

public class UserVehicle {
    User user;
    Vehicle vehicle;


    public UserVehicle(User user, Vehicle vehicle) {
        this.user = user;
        this.vehicle = vehicle;
    }

    public String toSerializedMap() {
        String userToString =  user.toSerializedMap();
        String vehicleToString = vehicle.toSerializedMap();

        return userToString.replace('}', ',') + vehicleToString.replace('{', ' ');
    }
}
