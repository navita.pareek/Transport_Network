package transporter.transporter_network.models;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class Trip {
    private String startDate;
    private String startState;
    private String startCity;
    private String endState;
    private String endCity;
    private String startStateName;
    private String startCityName;
    private String endStateName;
    private String endCityName;
    private String perVehicleCapacity;
    private String status;
    private String statusName;
    private String newStatus;
    private boolean visible;

    public void setStartStateName(String startStateName) {
        this.startStateName = startStateName;
    }

    public void setStartCityName(String startCityName) {
        this.startCityName = startCityName;
    }

    public void setEndStateName(String endStateName) {
        this.endStateName = endStateName;
    }

    public void setEndCityName(String endCityName) {
        this.endCityName = endCityName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getPerVehicleCapacity() {
        return perVehicleCapacity;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setStartState(String startState) {
        this.startState = startState;
    }

    public void setStartCity(String startCity) {
        this.startCity = startCity;
    }

    public void setEndState(String endState) {
        this.endState = endState;
    }

    public void setEndCity(String endCity) {
        this.endCity = endCity;
    }

    public void setPerVehicleCapacity(String perVehicleCapacity) {
        this.perVehicleCapacity = perVehicleCapacity;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    public String toSerializedMap() {
        StringBuilder result = new StringBuilder();
        try {
            result.append("{");
            result.append("\"" + CoreConstants.startDate + "\"");
            result.append(":");
            result.append("\"" + startDate + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.startState + "\"");
            result.append(":");
            result.append("\"" + startState + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.startCity + "\"");
            result.append(":");
            result.append("\"" + startCity + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.vehicleCapacityRequired + "\"");
            result.append(":");
            result.append("\"" + perVehicleCapacity + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.endState + "\"");
            result.append(":");
            result.append("\"" + endState + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.endCity + "\"");
            result.append(":");
            result.append("\"" + endCity + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.status + "\"");
            result.append(":");
            result.append("\"" + status + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.newStatus + "\"");
            result.append(":");
            result.append("\"" + newStatus + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.visible + "\"");
            result.append(":");
            result.append("\"" + visible + "\"");
            result.append("}");
        }catch(Exception ex){}
        return result.toString();
    }

    public String getTripSummary(String language) {
        StringBuilder result = new StringBuilder();
        try {
            result.append(LanguageStrings.getInstance().LOAD_TRIP_DATE.get(language) + ": " + startDate);
            result.append(" |\n");

            result.append(LanguageStrings.getInstance().START_LOCATION.get(language) + ": " +startStateName);
            result.append(":");

            result.append(startCityName);
            result.append(" |\n");

            result.append(LanguageStrings.getInstance().END_LOCATION.get(language) + ": " +endStateName);
            result.append(":");

            result.append( endCityName);
            result.append(" |\n");


            result.append(LanguageStrings.getInstance().VEHICLE_CAPACITY.get(language) + ": " +perVehicleCapacity + " " + TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().tonne));
            result.append(" |\n");


            result.append(LanguageStrings.getInstance().STATUS.get(language) + ": " +statusName);
            result.append("\n-------------------------------------");
        }catch(Exception ex){}
        return result.toString();
    }


    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
