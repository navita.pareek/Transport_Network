package transporter.transporter_network.models;

public class UserCompany {

    User user;
    Company company;

    public UserCompany(User user, Company company) {
        this.user = user;
        this.company = company;
    }

    public String toSerializedMap() {
        String userToString =  user.toSerializedMap();
        String companyToString = company.toSerializedMap();

        return userToString.replace('}', ',') + companyToString.replace('{', ' ');
    }
}
