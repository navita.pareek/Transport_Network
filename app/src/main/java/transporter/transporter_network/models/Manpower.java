package transporter.transporter_network.models;

import transporter.transporter_network.constants.CoreConstants;

public class Manpower {
    private String phoneNumber;
    private Boolean isEligibleToAddMember;

    public Manpower(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setIsEligibleToAddMember(Boolean isEligibleToAddMember) {
        this.isEligibleToAddMember = isEligibleToAddMember;
    }

    public String toSerializedMap() {
        StringBuilder result = new StringBuilder();
        try {
            result.append("{");
            result.append("\"" + CoreConstants.newManpowerPhoneNumber + "\"");
            result.append(":");
            result.append("\"" + phoneNumber + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.isEmpowerForStructureChange + "\"");
            result.append(":");
            result.append("\"" + isEligibleToAddMember + "\"");
            result.append("}");
        }catch(Exception ex){}
        return result.toString();
    }
}
