package transporter.transporter_network.models;

import transporter.transporter_network.constants.CoreConstants;

public class User {
    private String phoneNumber;
    private String dateOfBirth;
    private String password;
    private String language;
    private String oldPassword;
    private String referencePhoneNumber;

    public User(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    public User(String phoneNumber, String dateOfBirth, String password) {
        this.phoneNumber = phoneNumber;
        this.dateOfBirth = dateOfBirth;
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPassword() {
        return password;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public String toSerializedMap(){
        StringBuilder result = new StringBuilder();
    try {
        result.append("{");
        result.append("\"" + CoreConstants.phoneNumber + "\"");
        result.append(":");
        result.append("\"" + phoneNumber + "\"");
        result.append(",");

        result.append("\"" + CoreConstants.referencePhoneNumber + "\"");
        result.append(":");
        result.append("\"" + referencePhoneNumber + "\"");
        result.append(",");

        result.append("\"" + CoreConstants.dateOfBirth + "\"");
        result.append(":");
        result.append("\"" + dateOfBirth + "\"");
        result.append(",");

        result.append("\"" + CoreConstants.password + "\"");
        result.append(":");
        result.append("\"" + password + "\"");
        result.append(",");

        result.append("\"" + CoreConstants.oldPassword + "\"");
        result.append(":");
        result.append("\"" + oldPassword + "\"");
        result.append(",");

        result.append("\"" + CoreConstants.language + "\"");
        result.append(":");
        result.append("\"" + language + "\"");
        result.append("}");
    }catch(Exception ex){}
        return result.toString();
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setReferencePhoneNumber(String referencePhoneNumber) {
        this.referencePhoneNumber = referencePhoneNumber;
    }
}