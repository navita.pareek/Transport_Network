package transporter.transporter_network.models;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class Vehicle {
    private String vehicleNumber;
    private String capacity;
    private String startDate;
    private String startStateName;
    private String startCityName;
    private String endStateName;
    private String endCityName;
    private String startState;
    private String startCity;
    private String endState;
    private String endCity;
    private String currentState;
    private String currentCity;
    private String status;
    private String statusName;
    private Boolean isLinkedToCompany;

    public Vehicle(String vehicleNumber, String capacity){
        this.vehicleNumber = vehicleNumber;
        this.capacity = capacity;
    }

    public Vehicle(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public void setStartStateName(String startStateName) {
        this.startStateName = startStateName;
    }

    public void setStartCityName(String startCityName) {
        this.startCityName = startCityName;
    }

    public void setEndStateName(String endStateName) {
        this.endStateName = endStateName;
    }

    public void setEndCityName(String endCityName) {
        this.endCityName = endCityName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setStartState(String startState) {
        this.startState = startState;
    }
    public void setStartCity(String startCity) {
        this.startCity = startCity;
    }

    public void setEndState(String endState) {
        this.endState = endState;
    }
    public void setEndCity(String endCity) {
        this.endCity = endCity;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public void setCurrentCity(String currentCity) {
        this.currentCity = currentCity;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toSerializedMap() {
        StringBuilder result = new StringBuilder();
        try {
            result.append("{");
            result.append("\"" + CoreConstants.vehicleNumber + "\"");
            result.append(":");
            result.append("\"" + vehicleNumber + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.capacity + "\"");
            result.append(":");
            result.append("\"" + capacity + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.isLinkedToCompany + "\"");
            result.append(":");
            result.append("\"" + isLinkedToCompany + "\"");
            result.append(",");


            result.append("\"" + CoreConstants.startState + "\"");
            result.append(":");
            result.append("\"" + startState + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.startCity + "\"");
            result.append(":");
            result.append("\"" + startCity + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.currentState + "\"");
            result.append(":");
            result.append("\"" + currentState + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.currentCity + "\"");
            result.append(":");
            result.append("\"" + currentCity + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.endState + "\"");
            result.append(":");
            result.append("\"" + endState + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.endCity + "\"");
            result.append(":");
            result.append("\"" + endCity + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.status + "\"");
            result.append(":");
            result.append("\"" + status + "\"");
            result.append("}");
        }catch(Exception ex){}
        return result.toString();
    }

    public void setIsLinkedToCompany(Boolean isLinkedToCompany) {
        this.isLinkedToCompany = isLinkedToCompany;
    }

    public String getVehicleSummary(String language) {
        StringBuilder result = new StringBuilder();
        try {
            result.append(LanguageStrings.getInstance().VEHICLE_NUMBER.get(language) + ": " + vehicleNumber);
            result.append(" |\n");

            result.append(LanguageStrings.getInstance().START_LOCATION.get(language) + ": " + startStateName);
            result.append(":");

            result.append(startCityName);
            result.append(" |\n");

            result.append(LanguageStrings.getInstance().END_LOCATION.get(language) + ": " + endStateName);
            result.append(":");

            result.append( endCityName);
            result.append(" |\n");


            result.append(LanguageStrings.getInstance().VEHICLE_CAPACITY.get(language) + ": " + capacity + " " + TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().tonne));
            result.append(" |\n");


            result.append(LanguageStrings.getInstance().STATUS.get(language) + ": " + statusName);
            result.append("\n-------------------------------------");
        }catch(Exception ex){}
        return result.toString();
    }

    public String getStartState() {
        return startState;
    }

    public String getStartCity() {
        return startCity;
    }

    public String getEndCity() {
        return endCity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getCapacity() {
        return capacity;
    }

    public String getStatus() {
        return status;
    }

    public String getEndState() {
        return endState;
    }
}
