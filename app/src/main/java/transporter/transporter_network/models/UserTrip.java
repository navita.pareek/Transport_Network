package transporter.transporter_network.models;

public class UserTrip {
    private Trip trip;
    private User user;

    public UserTrip(User user, Trip trip) {
        this.user = user;
        this.trip = trip;
    }

    public String toSerializedMap() {
        String userToString =  user.toSerializedMap();
        String tripToString = trip.toSerializedMap();

        return userToString.replace('}', ',') + tripToString.replace('{', ' ');
    }
}
