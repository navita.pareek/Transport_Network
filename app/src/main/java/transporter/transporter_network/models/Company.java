package transporter.transporter_network.models;

import transporter.transporter_network.constants.CoreConstants;

public class Company {

    private String companyName;

    public Company(String companyName) {
        this.companyName = companyName;
    }

    public String toSerializedMap() {
        StringBuilder result = new StringBuilder();
        try {
            result.append("{");
            result.append("\"" + CoreConstants.companyName + "\"");
            result.append(":");
            result.append("\"" + companyName + "\"");
            result.append("}");
        }catch(Exception ex){}
        return result.toString();
    }
}
