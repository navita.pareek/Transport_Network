package transporter.transporter_network.models;

public class UserManpower {
    private Manpower manpower;
    private User user;

    public UserManpower(User user, Manpower manpower) {
        this.user = user;
        this.manpower = manpower;
    }


    public String toSerializedMap() {
        String userToString =  user.toSerializedMap();
        String manPowerToString = manpower.toSerializedMap();

        return userToString.replace('}', ',') + manPowerToString.replace('{', ' ');
    }
}
