package transporter.transporter_network.models;

import transporter.transporter_network.constants.CoreConstants;

public class CustomerFeedback {
    String phoneNumber;
    String isHappy;
    boolean wantToConnect;

    public CustomerFeedback(String phoneNumber, String isHappy){
        this.phoneNumber = phoneNumber;
        this.isHappy = isHappy;
    }

    public String toSerializedMap() {
        StringBuilder result = new StringBuilder();
        try {
            result.append("{");
            result.append("\"" + CoreConstants.phoneNumber + "\"");
            result.append(":");
            result.append("\"" + phoneNumber + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.isHappy + "\"");
            result.append(":");
            result.append("\"" + isHappy + "\"");
            result.append(",");

            result.append("\"" + CoreConstants.wantToConnect + "\"");
            result.append(":");
            result.append("\"" + wantToConnect + "\"");
            result.append("}");
        }catch(Exception ex){}
        return result.toString();
    }

    public void setWantToConnect(boolean wantToConnect) {
        this.wantToConnect = wantToConnect;
    }
}
