package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.Vehicle;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class UpdateVehicleStatusActivity extends AppCompatActivity {

    private String preferredLanguage;
    private int startCity = 0;
    private int endCity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_vehicle_status);

        getPreferredLanguage();
        getVehicleList();
        updateFormLabel();
        initCheckBoxesText();
        initUnCheckBoxes();
        initStartStateListener();
        initStartCityListener();
        initEndCityListener();
        initEndStateListener();
        updateCapacityHint();
        updateStatusSpinner();
        updateStateSpinner();
        updateCitySpinner();
        updateEndStateSpinner();
        updateEndCitySpinner();
        updateCitySpinner();
        updateButton();
    }

    private void initUnCheckBoxes() {
        ((CheckBox)findViewById(R.id.checkBox_any_start_state)).setChecked(false);
        ((CheckBox)findViewById(R.id.checkBox_any_start_city)).setChecked(false);
        ((CheckBox)findViewById(R.id.checkBox_any_end_state)).setChecked(false);
        ((CheckBox)findViewById(R.id.checkBox_any_end_city)).setChecked(false);
    }

    private void initCheckBoxesText() {
        ((CheckBox)findViewById(R.id.checkBox_any_start_state)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_start_state));
        ((CheckBox)findViewById(R.id.checkBox_any_start_city)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_start_city));
        ((CheckBox)findViewById(R.id.checkBox_any_end_state)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_end_state));
        ((CheckBox)findViewById(R.id.checkBox_any_end_city)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_end_city));
    }

    private void initEndCityListener() {
        CheckBox endCity = (CheckBox)findViewById(R.id.checkBox_any_end_city);
        endCity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner endCitySpinner = (Spinner) findViewById(R.id.spinner_end_city);
                if(isChecked) {
                    endCitySpinner.setSelection(0);
                    endCitySpinner.setEnabled(false);
                }else{
                    endCitySpinner.setEnabled(true);
                }
            }
        });
    }

    private void initStartCityListener() {
        CheckBox startCity = (CheckBox)findViewById(R.id.checkBox_any_start_city);
        startCity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner startCitySpinner = (Spinner) findViewById(R.id.spinner_current_city);
                if(isChecked) {
                    startCitySpinner.setSelection(0);
                    startCitySpinner.setEnabled(false);
                }else{
                    startCitySpinner.setEnabled(true);
                }
            }
        });
    }

    private void initStartStateListener() {
        CheckBox startState = (CheckBox)findViewById(R.id.checkBox_any_start_state);
        startState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner startStateSpinner = (Spinner) findViewById(R.id.spinner_current_state);
                Spinner startCitySpinner = (Spinner) findViewById(R.id.spinner_current_city);
                CheckBox startCityCheckbox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                if(isChecked) {
                    startStateSpinner.setSelection(0);
                    startStateSpinner.setEnabled(false);
                    startCityCheckbox.setChecked(false);
                    startCityCheckbox.setEnabled(false);
                    startCitySpinner.setSelection(0);
                    startCitySpinner.setEnabled(false);
                }else{
                    startStateSpinner.setEnabled(true);
                    startCitySpinner.setEnabled(true);
                    startCityCheckbox.setEnabled(true);
                }
            }
        });
    }

    private void initEndStateListener() {
        CheckBox endState = (CheckBox)findViewById(R.id.checkBox_any_end_state);
        endState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner endStateSpinner = (Spinner) findViewById(R.id.spinner_end_state);
                Spinner endCitySpinner = (Spinner) findViewById(R.id.spinner_end_city);
                CheckBox endCityCheckbox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                if(isChecked) {
                    endStateSpinner.setSelection(0);
                    endStateSpinner.setEnabled(false);
                    endCityCheckbox.setChecked(false);
                    endCityCheckbox.setEnabled(false);
                    endCitySpinner.setSelection(0);
                    endCitySpinner.setEnabled(false);
                }else{
                    endStateSpinner.setEnabled(true);
                    endCitySpinner.setEnabled(true);
                    endCityCheckbox.setEnabled(true);
                }
            }
        });
    }

    private void updateCapacityHint() {
        TextView textView = (TextView) findViewById(R.id.textView_vehicle_capacity_label);
        textView.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().new_vehicle_capacity_label));
        ((EditText) findViewById(R.id.editText_vehicle_capacity)).clearFocus();
    }

    private void updateButton() {
        Button button = (Button)findViewById(R.id.button_change_vehicle_status);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().update_truck_status_label));
    }

    private void updateFormLabel() {
        if(SeedData.getInstance().vehicles.size() == 0){
            TextView registerMeButton = (TextView) findViewById(R.id.TextView_search_result_label);
            registerMeButton.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no_vehicle_added));
            registerMeButton.setTextColor(Color.RED);
        }else {
            TextView registerMeButton = (TextView) findViewById(R.id.TextView_search_result_label);
            registerMeButton.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_update_vehicle_form_label));
            registerMeButton.setTextColor(Color.BLACK);
        }
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void updateVehicleNumberSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_vehicle_number);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_vehicle_number));
        for(int index = 0; index < SeedData.getInstance().vehicles.size(); index++)
            spinnerArray.add(SeedData.getInstance().vehicles.get(index).getVehicleNumber());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.textview, spinnerArray);
        spinner.setAdapter(adapter);
        spinner.setFocusable(true);
        spinner.setFocusableInTouchMode(true);
        spinner.requestFocus();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                updateFormLabel();
                initUnCheckBoxes();
                if(arg2 > 0) {
                    Vehicle vehicleSelected = SeedData.getInstance().vehicles.get(arg2 - 1);
                    startCity = Integer.parseInt(vehicleSelected.getStartCity());
                    endCity = Integer.parseInt(vehicleSelected.getEndCity());
                    ((TextView) findViewById(R.id.editText_vehicle_capacity)).setText(vehicleSelected.getCapacity());
                    ((Spinner) findViewById(R.id.spinner_current_state)).setSelection(Integer.parseInt(vehicleSelected.getStartState()));
                    ((Spinner) findViewById(R.id.spinner_current_status)).setSelection(Integer.parseInt(vehicleSelected.getStatus()));
                    ((Spinner) findViewById(R.id.spinner_end_state)).setSelection(Integer.parseInt(vehicleSelected.getEndState()));
                }else{
                    ((TextView) findViewById(R.id.editText_vehicle_capacity)).setText("");
                    ((Spinner) findViewById(R.id.spinner_current_state)).setSelection(0);
                    ((Spinner) findViewById(R.id.spinner_current_city)).setSelection(0);
                    ((Spinner) findViewById(R.id.spinner_current_status)).setSelection(0);
                    ((Spinner) findViewById(R.id.spinner_end_state)).setSelection(0);
                    ((Spinner) findViewById(R.id.spinner_end_city)).setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void updateCitySpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_current_city);
        String currentState = ((Spinner) findViewById(R.id.spinner_current_state)).getSelectedItem().toString();

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_vehicle_current_city));

        if(!currentState.equals(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_vehicle_current_state)))
            spinnerArray.addAll(SeedData.getInstance().stateCity.get(currentState));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
        ((Spinner) findViewById(R.id.spinner_current_city)).setSelection(this.startCity);
        this.startCity = 0;
    }

    private void updateStateSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_current_state);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_vehicle_current_state));

        for(Map.Entry<String, List<String>> entry : SeedData.getInstance().stateCity.entrySet()){
            spinnerArray.add(entry.getKey());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if(arg2 == 0){
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_current_city);
                    spinner.setVisibility(View.GONE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                    checkBox.setVisibility(View.GONE);
                }
                else{
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_current_city);
                    spinner.setVisibility(View.VISIBLE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                    checkBox.setVisibility(View.VISIBLE);
                }
                updateCitySpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void updateEndCitySpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
        String currentState = ((Spinner) findViewById(R.id.spinner_end_state)).getSelectedItem().toString();

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_city));

        if(!currentState.equals(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_state)))
            spinnerArray.addAll(SeedData.getInstance().stateCity.get(currentState));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
        ((Spinner) findViewById(R.id.spinner_end_city)).setSelection(this.endCity);
        this.endCity = 0;
    }

    private void updateEndStateSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_end_state);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_state));

        for(Map.Entry<String, List<String>> entry : SeedData.getInstance().stateCity.entrySet()){
            spinnerArray.add(entry.getKey());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if(arg2 == 0){
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
                    spinner.setVisibility(View.GONE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                    checkBox.setVisibility(View.GONE);
                }
                else{
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
                    spinner.setVisibility(View.VISIBLE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                    checkBox.setVisibility(View.VISIBLE);
                }
                updateEndCitySpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void updateStatusSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_current_status);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_vehicle_status));
        for(int index = 0; index < SeedData.getInstance().vehicleStatus.size(); index++) {
            spinnerArray.add(SeedData.getInstance().vehicleStatus.get(index));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
    }

    private void getVehicleList() {
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new UpdateVehicleStatusActivity.getUserVehicle(UpdateVehicleStatusActivity.this)).execute(user);
    }

    public void OnChangeVehicleStatusClicked(View view){
        int vehicleNumberSelected = ((Spinner) findViewById(R.id.spinner_vehicle_number)).getSelectedItemPosition();
        int currentState = ((Spinner) findViewById(R.id.spinner_current_state)).getSelectedItemPosition();
        int currentCity = ((Spinner) findViewById(R.id.spinner_current_city)).getSelectedItemPosition();
        int endState = ((Spinner) findViewById(R.id.spinner_end_state)).getSelectedItemPosition();
        int endCity = ((Spinner) findViewById(R.id.spinner_end_city)).getSelectedItemPosition();
        int status = ((Spinner) findViewById(R.id.spinner_current_status)).getSelectedItemPosition();
        String capacity = ((EditText) findViewById(R.id.editText_vehicle_capacity)).getText().toString();

        if(vehicleNumberSelected == 0 || capacity.trim().replace("0","").length() == 0){
            TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
            registerMeButton.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_update_vehicle_form_label));
            registerMeButton.setTextColor(Color.RED);
            return;
        }

        updateFormLabel();
        Vehicle vehicle = SeedData.getInstance().vehicles.get(vehicleNumberSelected - 1);

        vehicle.setCurrentState(Integer.toString(currentState));
        vehicle.setCurrentCity(Integer.toString(currentCity));
        vehicle.setEndState(Integer.toString(endState));
        vehicle.setEndCity(Integer.toString(endCity));
        vehicle.setCapacity(capacity);

        if(status != 0)
            vehicle.setStatus(Integer.toString(status));

        (new UpdateVehicleStatusActivity.updateVehicle(UpdateVehicleStatusActivity.this)).execute(vehicle);
    }

    private class updateVehicle extends AsyncTask<Vehicle, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private updateVehicle(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Vehicle... vehicle) {
            String result = null;
            for(int index = 0; index < vehicle.length; index++){
                result = CoreAdapter.updateVehicle(vehicle[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result.equals(CoreConstants.SUCCESS)){
                Toast.makeText(activity, LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
                ((Spinner) findViewById(R.id.spinner_vehicle_number)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_current_state)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_current_city)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_end_state)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_end_city)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_current_status)).setSelection(0);
                ((EditText) findViewById(R.id.editText_vehicle_capacity)).setText("");
                getVehicleList();

            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class getUserVehicle extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;
        private String userPhoneNumber;

        private getUserVehicle(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.getUserVehicle(user[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().VEHICLES_KEY, result);
                SeedData.getInstance().populateVehiclesMap();
                updateVehicleNumberSpinner();
                updateFormLabel();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
