package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.CustomerFeedback;
import transporter.transporter_network.models.User;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class DashboardActivity extends AppCompatActivity {

    private String preferredLanguage;
    private boolean doNeedCallBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        getPreferredLanguage();
        setSearchTruckLabel();
        setSearchLoadLabel();
        setMyCompanyLabel();
        setMyVehiclesLabel();
        setMyTripsLabel();
        setUserId();
        setCustomerSupportLabel();
        setLogoutLabel();

    }

    public void onUserIdClicked(View view){
        (new DashboardActivity.getUserTnPoints(DashboardActivity.this)).execute(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
    }

    private void setUserId() {
        EditText editText = (EditText)findViewById(R.id.editText_user_id);
        editText.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
    }

    private void setCustomerSupportLabel() {
        EditText editText = (EditText)findViewById(R.id.editText_contact_support);
        editText.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().contact_support_label));
    }


    public void onContactUserSupportClick(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
        builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().contact_user_support));
        builder.setPositiveButton(
                LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
                        CustomerFeedback customerFeedback = new CustomerFeedback(phoneNumber, LanguageStrings.getInstance().CUSTOMER_SUPPORT);
                        customerFeedback.setWantToConnect(true);
                        (new DashboardActivity.updateCustomerFeedback(DashboardActivity.this)).execute(customerFeedback);
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setMyTripsLabel() {
        Button button = (Button) findViewById(R.id.button_my_trips);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().my_trips_label));
    }

    public void onMyCompanyClick(View view){
        (new getUserCompany(DashboardActivity.this)).execute();
    }

    public void onMyVehiclesClick(View view){
        Intent intent = new Intent(DashboardActivity.this, MyVehiclesActivity.class);
        startActivity(intent);
    }

    public void onMyTripsClick(View view){
        Intent intent = new Intent(DashboardActivity.this, MyTripsActivity.class);
        startActivity(intent);
    }

    public void onStarClick(View view){
        View checkBoxView = View.inflate(this, R.layout.checkbox, null);
        CheckBox checkBox = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                doNeedCallBack = isChecked;
            }
        });
        checkBox.setChecked(true);
        checkBox.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().want_team_to_call));

        AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
        builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().customer_happy));
        builder.setCancelable(false);
        builder.setView(checkBoxView);
        builder.setNegativeButton(
            TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no),
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    sendFeedback(false);
                    dialog.cancel();
                }
        });
        builder.setPositiveButton(
                TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sendFeedback(true);
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void sendFeedback(boolean isHappy){
        String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
        String msg = (isHappy) ? LanguageStrings.getInstance().HAPPY : LanguageStrings.getInstance().NEED_IMPROVEMENT;
        CustomerFeedback customerFeedback = new CustomerFeedback(phoneNumber, msg);
        customerFeedback.setWantToConnect(doNeedCallBack);
        (new DashboardActivity.updateCustomerFeedback(DashboardActivity.this)).execute(customerFeedback);
    }

    public void OnLanguageButtonClick(View view) {
        Button b = (Button)view;
        String buttonContentDescription = b.getContentDescription().toString();

        if(!this.preferredLanguage.equals(buttonContentDescription.toUpperCase())) {
            updatedPreferredLanguage(buttonContentDescription.toUpperCase());
            updateUserLanguage();
            updateAllControlsLabel();
        }

        Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().dashboard_language_set_to.get(preferredLanguage), Toast.LENGTH_SHORT).show();
    }

    private void getSeedData(){
        (new DashboardActivity.getStringConstantsForUser(DashboardActivity.this)).execute(preferredLanguage);
        (new DashboardActivity.getStatesCities(DashboardActivity.this)).execute();
        (new DashboardActivity.getVehicleStatus(DashboardActivity.this)).execute();
        (new DashboardActivity.getTripStatus(DashboardActivity.this)).execute();
    }

    private void updateAllControlsLabel() {
        getSeedData();
    }

    private void updateUserLanguage(){
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        user.setLanguage(preferredLanguage);
        (new updateUserLanguage(DashboardActivity.this)).execute(user);
    }

    private void updatedPreferredLanguage(String preferredLanguage) {
        TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY, preferredLanguage);
        this.preferredLanguage = preferredLanguage;
    }

    public void OnSearchLoadClick(View view){
        Intent intent = new Intent(DashboardActivity.this, SearchLoadActivity.class);
        startActivity(intent);
    }

    public void onLogOutClick(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
        builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().logout_confirmation));
        builder.setCancelable(false);
        builder.setNegativeButton(
                TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.setPositiveButton(
                TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_REGISTER_ID, null);
                        TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY, null);

                        Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    public void onSearchVehicleClick(View view){
        Intent intent = new Intent(DashboardActivity.this, SearchVehicleActivity.class);
        startActivity(intent);
    }

    private void setLogoutLabel() {
        Button button = (Button) findViewById(R.id.button_logout);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().logout_label));
    }

    private void setMyCompanyLabel(){
        Button button = (Button) findViewById(R.id.button_my_company);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().my_company_label));
    }

    private void setMyVehiclesLabel(){
        Button button = (Button) findViewById(R.id.button_my_vehicles);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().my_vehicles_label));
    }

    private void setSearchLoadLabel() {
        Button button = (Button) findViewById(R.id.button_search_load);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().search_load_label));
    }

    private void setSearchTruckLabel() {
        Button button = (Button) findViewById(R.id.button_search_truck);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().search_truck_label));
    }

    private class updateCustomerFeedback extends AsyncTask<CustomerFeedback, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private updateCustomerFeedback(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(CustomerFeedback... params) {
            String result = null;
            for(int i = 0; i < params.length; i++)
                result = CoreAdapter.addCustomerFeedback(params[i]);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                Toast.makeText(activity, LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class updateUserLanguage extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private updateUserLanguage(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... params) {
            String result = null;
            for(int i = 0; i < params.length; i++)
                result = CoreAdapter.updateUserLanguage(params[i]);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getUserTnPoints extends AsyncTask<String, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getUserTnPoints(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(String... userPhoneNumber) {
            String result = null;
            for(int index = 0; index < userPhoneNumber.length; index++){
                result = CoreAdapter.getUserTnPoints(userPhoneNumber[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().user_tn_points)  + ": " + result + "\n\n" + TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().tn_point_definition));
                builder.setCancelable(false);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getStringConstantsForUser extends AsyncTask<String, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getStringConstantsForUser(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(String... language) {
            String result = null;
            for(int index = 0; index < language.length; index++){
                result = CoreAdapter.getStringConstants(language[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                LanguageStrings.getInstance().populateConstantsMap(result);
                setSearchTruckLabel();
                setSearchLoadLabel();
                setMyCompanyLabel();
                setMyTripsLabel();
                setMyVehiclesLabel();
                setCustomerSupportLabel();
                setLogoutLabel();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getStatesCities extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getStatesCities(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getStatesCities(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                LanguageStrings.getInstance().populateStatesCitiesMap(result);
                SeedData.getInstance().populateStateCityMap();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getVehicleStatus extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getVehicleStatus(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getVehicleStatus(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                SeedData.getInstance().populateVehicleStatusList(result);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getTripStatus extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getTripStatus(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getTripStatus(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                SeedData.getInstance().populateTripStatusList(result);

            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getUserCompany extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getUserCompany(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getUserCompany(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_COMPANY_NAME, result);
                if(result == null || result.length() == 0){
                    Intent intent = new Intent(DashboardActivity.this, AddCompanyNameActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(DashboardActivity.this, MyCompanyActivity.class);
                    startActivity(intent);
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }
}
