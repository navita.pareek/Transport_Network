package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.Trip;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserTrip;
import transporter.transporter_network.models.Vehicle;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class MyWorkActivity extends AppCompatActivity {

    private String preferredLanguage;
    private String searchEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_work);

        getPreferredLanguage();

        searchEntity = getIntent().getStringExtra(CoreConstants.SearchEntity);
        if(searchEntity.equals(CoreConstants.SearchLoadForVehicle)){
            setSelectVehicle();
            MyWorkActivity.this.setTitle("My Vehicles >> Search Load for added Vehicle");
            getVehicleList();
        }else {
            setSelectTrip();
            MyWorkActivity.this.setTitle("My Trips >> Search Vehicle for added Load");
            getUserTrips();
        }
    }

    private void setSelectTrip() {
        ((TextView)findViewById(R.id.TextView_select_from_spinner_label)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_trip_from_spinner));
    }

    private void setSelectVehicle() {
        ((TextView)findViewById(R.id.TextView_select_from_spinner_label)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_vehicle_from_spinner));
    }

    private void updateVehicleNumberSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_work);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_vehicle_number));
        for(int index = 0; index < SeedData.getInstance().vehicles.size(); index++) {

            spinnerArray.add((index +1 ) + ".\n" +
                    SeedData.getInstance().vehicles.get(index).getVehicleSummary(preferredLanguage));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.textview, spinnerArray);
        spinner.setAdapter(adapter);
        spinner.setFocusable(true);
        spinner.setFocusableInTouchMode(true);
        spinner.requestFocus();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if(arg2 > 0) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    String startDate = dateFormat.format(Calendar.getInstance().getTime());

                    Vehicle vehicleSelected = SeedData.getInstance().vehicles.get(arg2 - 1);
                    Trip trip = new Trip();
                    trip.setStartDate(startDate);
                    trip.setStartState(vehicleSelected.getStartState());
                    trip.setStartCity(vehicleSelected.getStartCity());
                    trip.setEndState(vehicleSelected.getEndState());
                    trip.setEndCity(vehicleSelected.getEndCity());
                    trip.setPerVehicleCapacity(vehicleSelected.getCapacity());

                    String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
                    User user = new User(phoneNumber);

                    UserTrip userTrip = new UserTrip(user, trip);
                    (new searchLoadForVehicle(MyWorkActivity.this)).execute(userTrip);
                }else{
                    ((TextView) findViewById(R.id.TextView_work)).setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void updateTripsSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_work);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_trip));

        for(int index = 0; index < SeedData.getInstance().trips.size(); index++) {
            spinnerArray.add((index +1 ) + ".\n" + SeedData.getInstance().trips.get(index).getTripSummary(preferredLanguage));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.textview, spinnerArray);
        spinner.setAdapter(adapter);
        spinner.setFocusable(true);
        spinner.setFocusableInTouchMode(true);
        spinner.requestFocus();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                String selCat = spinner.getItemAtPosition(arg2).toString();
                if(arg2 > 0) {
                    Trip tripSelected = SeedData.getInstance().trips.get(arg2 - 1);
                    tripSelected.setVisible(false);

                    String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
                    User user = new User(phoneNumber);

                    UserTrip userTrip = new UserTrip(user, tripSelected);
                    (new MyWorkActivity.searchVehicle(MyWorkActivity.this)).execute(userTrip);
                }else{
                    ((TextView) findViewById(R.id.TextView_work)).setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void getVehicleList() {
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new MyWorkActivity.getUserVehicle(MyWorkActivity.this)).execute(user);
    }

    public void getUserTrips() {
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new MyWorkActivity.getUserTrips(MyWorkActivity.this)).execute(user);
    }

    private void populateResult(String result) {
        if(result.length() == 0){
            ((TextView) findViewById(R.id.TextView_work)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no_result_found));
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();
        try {
            JSONObject mainJsonObject = new JSONObject(result);
            JSONArray interimArray = mainJsonObject.getJSONArray(CoreConstants.users);

            if(interimArray.length() == 0){
                ((TextView) findViewById(R.id.TextView_work)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no_result_found));
                return;
            }

            stringBuilder.append("\n" + TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().title_activity_search_result) + "\n\n");
            stringBuilder.append("Company Name : Contact Number : TN points\n");
            stringBuilder.append("-------------------------------------------------------------\n");
            for (int index = 0; index < interimArray.length(); index++) {
                JSONObject jsonObject = interimArray.getJSONObject(index);
                String phoneNumber = jsonObject.getString("phoneNumber");
                String companyName = jsonObject.getString("companyName");
                String userTnPoints = jsonObject.getString("userTnPoints");

                if(stringBuilder.indexOf(phoneNumber) < 0){
                    stringBuilder.append(companyName + " : " + phoneNumber  + " : " + userTnPoints + "\n");
                    stringBuilder.append("...............................................\n");
                }
            }
        }catch (Exception ex){
            ((TextView) findViewById(R.id.TextView_work)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no_result_found));
            return;
        }

        ((TextView) findViewById(R.id.TextView_work)).setText(stringBuilder.toString());
    }

    private class getUserVehicle extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getUserVehicle(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.getUserVehicle(user[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().VEHICLES_KEY, result);
                SeedData.getInstance().populateVehiclesMap();
                updateVehicleNumberSpinner();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class getUserTrips extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getUserTrips(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.getUserTrips(user[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().TRIPS_KEY, result);
                SeedData.getInstance().populateTripsMap();
                updateTripsSpinner();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class searchLoadForVehicle extends AsyncTask<UserTrip, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private searchLoadForVehicle(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserTrip... userTrip) {
            String result = null;
            for(int index = 0; index < userTrip.length; index++){
                result = CoreAdapter.searchLoadForVehicle(userTrip[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                populateResult(result);
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class searchVehicle extends AsyncTask<UserTrip, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private searchVehicle(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserTrip... userTrip) {
            String result = null;
            for(int index = 0; index < userTrip.length; index++){
                result = CoreAdapter.searchVehicleForTrip(userTrip[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                populateResult(result);
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
