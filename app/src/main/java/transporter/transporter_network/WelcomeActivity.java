package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class WelcomeActivity extends AppCompatActivity {

    private String preferredLanguage;
    private String userRegisteredId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        getPreferredLanguage();
        getUserRegisteredId();
        setOkButtonText();
        setWelcomeMessage();

        (new getStringConstantsForUser(WelcomeActivity.this)).execute(preferredLanguage);
        (new getStatesCities(WelcomeActivity.this)).execute();
        (new getVehicleStatus(WelcomeActivity.this)).execute();
        (new getTripStatus(WelcomeActivity.this)).execute();
        (new getUserCompany(WelcomeActivity.this)).execute();
        (new getIsUserEmpower(WelcomeActivity.this)).execute();
    }

    private void setOkButtonText() {
        Button button = (Button) findViewById(R.id.button_ok);
        button.setText(LanguageStrings.getInstance().ok_label.get(preferredLanguage));
    }

    private void setWelcomeMessage() {
        TextView welcomeMessage = (TextView) findViewById(R.id.welcome_message);
        if(isUserRegisteredIdInvalid()){
            welcomeMessage.setText(LanguageStrings.getInstance().registration_failed_msg.get(preferredLanguage));
            welcomeMessage.setTextColor(Color.RED);
            return;
        }

        welcomeMessage.setText(LanguageStrings.getInstance().welcome_message.get(preferredLanguage));
        welcomeMessage.setTextColor(Color.BLACK);

        TextView disclaimer = (TextView) findViewById(R.id.TextView_disclaimer_label);
        disclaimer.setText(LanguageStrings.getInstance().disclaimer_message.get(preferredLanguage));
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void getUserRegisteredId() {
         userRegisteredId = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
    }

    private boolean isUserRegisteredIdInvalid() {
        return (userRegisteredId == null) ||
                (userRegisteredId.length() < 1);
    }

    public void OnOkButtonClicked(View view){

        if(isUserRegisteredIdInvalid()){
            Intent intent = new Intent(WelcomeActivity.this, RegisterUserActivity.class);
            startActivity(intent);
        }else{
            Intent intent = new Intent(WelcomeActivity.this, DashboardActivity.class);
            startActivity(intent);
        }
    }

    private class getStringConstantsForUser extends AsyncTask<String, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getStringConstantsForUser(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(String... language) {
            String result = null;
            for(int index = 0; index < language.length; index++){
                result = CoreAdapter.getStringConstants(language[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                LanguageStrings.getInstance().populateConstantsMap(result);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class getStatesCities extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getStatesCities(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getStatesCities(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                LanguageStrings.getInstance().populateStatesCitiesMap(result);
                SeedData.getInstance().populateStateCityMap();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getVehicleStatus extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getVehicleStatus(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getVehicleStatus(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                SeedData.getInstance().populateVehicleStatusList(result);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getTripStatus extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getTripStatus(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getTripStatus(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                SeedData.getInstance().populateTripStatusList(result);

            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getUserCompany extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getUserCompany(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getUserCompany(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_COMPANY_NAME, result);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }

    private class getIsUserEmpower extends AsyncTask<Void, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getIsUserEmpower(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result;
            String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
            result = CoreAdapter.getIsUserEmpowered(phoneNumber);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                TransporterSharedPreferences.getInstance().setString(CoreConstants.USER_IS_EMPOWERED, result);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }
}
