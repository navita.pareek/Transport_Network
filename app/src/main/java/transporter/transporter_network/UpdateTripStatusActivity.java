package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.Trip;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserTrip;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class UpdateTripStatusActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_trip_status);

        getPreferredLanguage();
        updateFormLabel();
        getUserTrips();
        updateStatusSpinner();
        updateButton();
    }

    private void updateButton() {
        Button button = (Button)findViewById(R.id.button_update_trip_status);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().update_trip_status_label));
    }

    private void updateFormLabel() {
        TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
        registerMeButton.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_form_update_trip_status_label));
        registerMeButton.setTextColor(Color.BLACK);
    }

    private void updateStatusSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_trip_status);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_trip_status));
        for(int index = 0; index < SeedData.getInstance().tripStatus.size(); index++) {
            spinnerArray.add(SeedData.getInstance().tripStatus.get(index));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
    }

    private void updateTripsSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_trips);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_trip));

        for(int index = 0; index < SeedData.getInstance().trips.size(); index++) {
            spinnerArray.add(SeedData.getInstance().trips.get(index).getTripSummary(preferredLanguage));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.textview, spinnerArray);
        spinner.setAdapter(adapter);
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    public void OnUpdateTripStatusClicked(View view){
        int tripDetailsSelected = ((Spinner) findViewById(R.id.spinner_trips)).getSelectedItemPosition();
        int newStatus = ((Spinner) findViewById(R.id.spinner_trip_status)).getSelectedItemPosition();

        if(tripDetailsSelected == 0 || newStatus == 0){
            TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
            registerMeButton.setText(LanguageStrings.getInstance().fill_required_field_label.get(preferredLanguage));
            registerMeButton.setTextColor(Color.RED);
            return;
        }

        Trip trip = SeedData.getInstance().trips.get(tripDetailsSelected - 1);
        trip.setNewStatus(Integer.toString(newStatus));
        updateFormLabel();
        String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
        User user = new User(phoneNumber);

        UserTrip userTrip = new UserTrip(user, trip);
        (new UpdateTripStatusActivity.updateTrip(UpdateTripStatusActivity.this)).execute(userTrip);
    }

    public void getUserTrips() {
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new UpdateTripStatusActivity.getUserTrips(UpdateTripStatusActivity.this)).execute(user);
    }

    private class getUserTrips extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getUserTrips(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.getUserTrips(user[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().TRIPS_KEY, result);
                SeedData.getInstance().populateTripsMap();
                updateTripsSpinner();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private class updateTrip extends AsyncTask<UserTrip, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private updateTrip(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserTrip... userTrip) {
            String result = null;
            for(int index = 0; index < userTrip.length; index++){
                result = CoreAdapter.updateTrip(userTrip[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result.equals(CoreConstants.SUCCESS)){
                Toast.makeText(activity, LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
                ((Spinner) findViewById(R.id.spinner_trips)).setSelection(0);
                ((Spinner) findViewById(R.id.spinner_trip_status)).setSelection(0);
                getUserTrips();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
