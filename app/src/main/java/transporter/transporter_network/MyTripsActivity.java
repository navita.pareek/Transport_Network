package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.User;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class MyTripsActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trips);

        getPreferredLanguage();
        getTripList();
        setAddTripLabel();
        setUpdateTripStatus();
        setSearchVehicleForTripButtonLabel();
    }

    @Override
    public void onResume(){
        super.onResume();
        getTripList();
    }

    private void setSearchVehicleForTripButtonLabel() {
        Button button = (Button) findViewById(R.id.button_search_vehicle_for_added_trips);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().search_vehicle_for_added_trips_label));
    }

    public void OnSearchVehicleForAddedTripsClick(View view){
        Intent intent = new Intent(MyTripsActivity.this, MyWorkActivity.class);
        intent.putExtra(CoreConstants.SearchEntity, CoreConstants.SearchVehicleForTrips);
        startActivity(intent);
    }

    private void setUpdateTripStatus() {
        Button button = (Button) findViewById(R.id.button_update_trip_status);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().update_trip_status_label));
    }

    public void OnUpdateTripStatusClicked(View view){
        Intent intent = new Intent(MyTripsActivity.this, UpdateTripStatusActivity.class);
        startActivity(intent);
    }

    public void OnAddTripClick(View view){
        Intent intent = new Intent(MyTripsActivity.this, AddTripActivity.class);
        startActivity(intent);
    }

    private void getTripList() {
        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        (new MyTripsActivity.getUserTrips(MyTripsActivity.this)).execute(user);
    }

    private void setTripListLabel() {
        TextView text = (TextView) findViewById(R.id.TextView_my_trips_list_label);
        if(SeedData.getInstance().trips.size() == 0) {
            text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().no_trip_added));
        }
        else{
            String VehicleCountMessage = SeedData.getInstance().trips.size() + " " + TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().trips_added);
            text.setText(VehicleCountMessage);
        }
    }

    private void setTripsList() {
        TextView text = (TextView) findViewById(R.id.TextView_my_trips);
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < SeedData.getInstance().trips.size(); i++){
            result.append((i+1) + ".\n");
            result.append(SeedData.getInstance().trips.get(i).getTripSummary(preferredLanguage) + "\n");
        }
        text.setText(result.toString());
    }

    private void setAddTripLabel() {
        Button button = (Button) findViewById(R.id.button_add_trip);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().add_trip_label));
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void hideActionButtonsIfTripListEmpty() {
        if(SeedData.getInstance().trips.size() == 0) {
            Button button = (Button) findViewById(R.id.button_update_trip_status);
            button.setVisibility(View.GONE);
            button = (Button) findViewById(R.id.button_search_vehicle_for_added_trips);
            button.setVisibility(View.GONE);
        }
        else{
            Button button = (Button) findViewById(R.id.button_update_trip_status);
            button.setVisibility(View.VISIBLE);
            button = (Button) findViewById(R.id.button_search_vehicle_for_added_trips);
            button.setVisibility(View.VISIBLE);
        }
    }

    private class getUserTrips extends AsyncTask<User, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private getUserTrips(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(User... user) {
            String result = null;
            for(int index = 0; index < user.length; index++){
                result = CoreAdapter.getUserTrips(user[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().TRIPS_KEY, result);
                SeedData.getInstance().populateTripsMap();
                hideActionButtonsIfTripListEmpty();
                setTripListLabel();
                setTripsList();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
