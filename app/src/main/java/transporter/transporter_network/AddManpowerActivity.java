package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.Manpower;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserManpower;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class AddManpowerActivity extends AppCompatActivity {

    private String preferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_manpower);

        getPreferredLanguage();
        setFillFormLabel();
        setCheckboxLabel();
        setNewMemberPhoneNumberTextHint();
        setAddManpowerButtonText();
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void setAddManpowerButtonText() {
        Button button = (Button) findViewById(R.id.button_add_manpower);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().add_man_power_label));
    }

    private void setNewMemberPhoneNumberTextHint() {
        TextView text = (TextView) findViewById(R.id.textView_new_number_label);
        text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().new_member_phone_number_label));

        EditText text1 = (EditText) findViewById(R.id.editText_new_member_phone_number);
        text1.setHint(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().phone_number_hint.get(preferredLanguage)));
    }

    private void setCheckboxLabel() {
        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_is_empowered_to_add_other_manpower);
        checkBox.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().is_empower_to_change_structure));
    }

    private void setFillFormLabel() {
        TextView text = (TextView) findViewById(R.id.TextView_fill_add_company_form_label);
        text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_add_member_form_label));
    }

    private boolean areAllRequiredFieldsFilled() {
        return (((EditText) findViewById(R.id.editText_new_member_phone_number)).getText().toString().trim().length() != 0);
    }

    private boolean isValidNewMemberPhoneNumber() {
        EditText text = (EditText) findViewById(R.id.editText_new_member_phone_number);
        if(text.getText().toString().equals(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID)))
            return false;
        return text.getText().toString().trim().length() > 9;
    }

    private boolean allUserInputsAreValid() {
        TextView textView = (TextView)findViewById(R.id.TextView_fill_add_company_form_label);

        if(!areAllRequiredFieldsFilled()){
            textView.setText(LanguageStrings.getInstance().fill_required_field_label.get(preferredLanguage));
            textView.setTextColor(Color.RED);
            return false;
        }

        if(!isValidNewMemberPhoneNumber()){
            textView.setText(LanguageStrings.getInstance().invalid_phone_number_label.get(preferredLanguage));
            textView.setTextColor(Color.RED);
            return false;
        }

        return true;
    }

    public void OnAddManpowerSubmitClicked(View view){
        TextView text = (TextView) findViewById(R.id.TextView_fill_add_company_form_label);
        text.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_add_member_form_label));
        text.setTextColor(Color.BLACK);

        if(allUserInputsAreValid()){
            try {
                addManpower();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void addManpower() {
        String newMemberPhoneNumber = ((EditText) findViewById(R.id.editText_new_member_phone_number)).getText().toString();
        Boolean isEligibleToAddMember = ((CheckBox) findViewById(R.id.checkBox_is_empowered_to_add_other_manpower)).isChecked();

        Manpower manpower = new Manpower(newMemberPhoneNumber);
        manpower.setIsEligibleToAddMember(isEligibleToAddMember);

        User user = new User(TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID));
        UserManpower userManpower = new UserManpower(user, manpower);

        (new AddManpowerActivity.addManpower(AddManpowerActivity.this)).execute(userManpower);
    }

    private class addManpower extends AsyncTask<UserManpower, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private addManpower(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserManpower... userManpower) {
            String result = null;
            for(int index = 0; index < userManpower.length; index++){
                result = CoreAdapter.addManpower(userManpower[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("TRUE")){
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().request_succeeded_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
                ((EditText) findViewById(R.id.editText_new_member_phone_number)).setText("");
                ((CheckBox) findViewById(R.id.checkBox_is_empowered_to_add_other_manpower)).setChecked(false);
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
