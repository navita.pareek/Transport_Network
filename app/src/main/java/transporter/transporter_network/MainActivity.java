package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.CustomerFeedback;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class MainActivity extends AppCompatActivity {

    private String userPreferredLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initSharedPreference();
        checkIfNeedToGoToAnotherActivity();
    }

    public void onContactUserSupportClick(View view){
        View editTextView = View.inflate(this, R.layout.edit_text, null);
        final EditText textBox = (EditText) editTextView.findViewById(R.id.editText);
        textBox.setHint(LanguageStrings.getInstance().phone_number_hint.get(userPreferredLanguage));

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(LanguageStrings.getInstance().forget_password.get(userPreferredLanguage));
        builder.setView(editTextView);
        builder.setCancelable(true);
        builder.setPositiveButton(
                LanguageStrings.getInstance().ok_label.get(userPreferredLanguage),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        String phoneNumber = textBox.getText().toString();
                        CustomerFeedback customerFeedback = new CustomerFeedback(phoneNumber, LanguageStrings.getInstance().NEW_CUSTOMER_SUPPORT);
                        customerFeedback.setWantToConnect(true);
                        (new MainActivity.updateCustomerFeedback(MainActivity.this)).execute(customerFeedback);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setCustomerSupportLabel() {
        EditText editText = (EditText)findViewById(R.id.editText_contact_support);
        editText.setText(LanguageStrings.getInstance().contact_support_text.get(userPreferredLanguage));
    }

    private void checkIfNeedToGoToAnotherActivity() {
        if(userAlreadyRegistered()){
            Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
            startActivity(intent);
        }else{
            userPreferredLanguage = "ENGLISH";
            TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY, userPreferredLanguage);
        }
    }

    private boolean userAlreadyRegistered() {
        return TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID) != null;
    }

    private void initSharedPreference() {
        TransporterSharedPreferences.getInstance()
                .setSharedPreference(getSharedPreferences(getResources().getString(R.string.shared_preference_file_name), MODE_PRIVATE));
    }

    public void OnButtonClick(View view) {
        Button b = (Button)view;
        String buttonContentDescription = b.getContentDescription().toString();

        updatedPreferredLanguage(buttonContentDescription.toUpperCase());
        updateRegisterButtonText();
        updateLoginButtonText();
        updateChooseLanguageLabelText();
        setCustomerSupportLabel();
        Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().main_page_language_set_to.get(userPreferredLanguage), Toast.LENGTH_SHORT).show();
    }

    private void updateLoginButtonText() {
        Button registerMeButton = (Button)findViewById(R.id.button_login);
        registerMeButton.setText(LanguageStrings.getInstance().new_user_login_button_label.get(userPreferredLanguage));
    }

    private void updatedPreferredLanguage(String preferredLanguage) {
        TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY, preferredLanguage);
        userPreferredLanguage = preferredLanguage;
    }

    private void updateChooseLanguageLabelText() {
        TextView registerMeButton = (TextView)findViewById(R.id.label_choose_language);
        registerMeButton.setText(LanguageStrings.getInstance().chose_preferred_language_label.get(userPreferredLanguage));
    }

    public void OnRegisterUserButtonClick(View view) {
        if(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY) == null)
            TransporterSharedPreferences.getInstance().setString(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY, "ENGLISH");

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(LanguageStrings.getInstance().points_rules.get(userPreferredLanguage));
        builder.setCancelable(false);
        builder.setPositiveButton(
                LanguageStrings.getInstance().ok_label.get(userPreferredLanguage),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent intent = new Intent(MainActivity.this, RegisterUserActivity.class);
                        startActivity(intent);
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void OnLoginButtonClick(View view) {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private void updateRegisterButtonText() {
        Button registerMeButton = (Button)findViewById(R.id.button_register_me);
        registerMeButton.setText(LanguageStrings.getInstance().open_register_user_activity_label.get(userPreferredLanguage));
    }

    private class updateCustomerFeedback extends AsyncTask<CustomerFeedback, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private updateCustomerFeedback(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(CustomerFeedback... params) {
            String result = null;
            for(int i = 0; i < params.length; i++)
                result = CoreAdapter.addCustomerFeedback(params[i]);
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null) {
                Toast.makeText(activity, LanguageStrings.getInstance().request_succeeded_toast.get(userPreferredLanguage), Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if (result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (userPreferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (userPreferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                } else {
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(userPreferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(userPreferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            }
        }
    }
}
