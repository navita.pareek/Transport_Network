package transporter.transporter_network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import transporter.transporter_network.constants.CoreConstants;
import transporter.transporter_network.constants.LanguageStrings;
import transporter.transporter_network.models.Trip;
import transporter.transporter_network.models.User;
import transporter.transporter_network.models.UserTrip;
import transporter.transporter_network.tools.CoreAdapter;
import transporter.transporter_network.tools.SeedData;
import transporter.transporter_network.tools.TransporterSharedPreferences;

public class SearchLoadActivity extends AppCompatActivity {

    private String preferredLanguage;
    final private Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_load);

        getPreferredLanguage();
        setStartDateLabel();
        setTodayDate();
        setLoadWeightLabel();
        initCheckBoxesText();
        initStartStateListener();
        initStartCityListener();
        initEndCityListener();
        initEndStateListener();
        updateFormLabelText();
        updateStartStateSpinner();
        updateStartCitySpinner();
        updateEndStateSpinner();
        updateEndCitySpinner();
        updateSearchLoadButtonText();
    }

    public void onButtonNextDateClicked(View view) throws ParseException {
        EditText date = (EditText)findViewById(R.id.editText_start_date);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse(date.getText().toString()));
        cal.add(Calendar.DATE, 1);
        String increasedDate = dateFormat.format(cal.getTime());

        date.setText(increasedDate);
    }


    public void onButtonPreDateClicked(View view) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String todayDate = dateFormat.format(Calendar.getInstance().getTime());
        EditText date = (EditText)findViewById(R.id.editText_start_date);

        if(todayDate.equals(date.getText().toString())){
            Toast.makeText(getApplicationContext(), TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().today_date_is_min_date), Toast.LENGTH_SHORT).show();
            return;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse(date.getText().toString()));
        cal.add(Calendar.DATE, -1);
        String reducedDate = dateFormat.format(cal.getTime());
        date.setText(reducedDate);
    }

    private void setTodayDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String todayDate = dateFormat.format(Calendar.getInstance().getTime());
        ((EditText) findViewById(R.id.editText_start_date)).setText(todayDate);
    }

    private void initCheckBoxesText() {
        ((CheckBox)findViewById(R.id.checkBox_any_start_state)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_start_state));
        ((CheckBox)findViewById(R.id.checkBox_any_start_city)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_start_city));
        ((CheckBox)findViewById(R.id.checkBox_any_end_state)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_end_state));
        ((CheckBox)findViewById(R.id.checkBox_any_end_city)).setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().chkBox_any_end_city));
    }

    private void initEndCityListener() {
        CheckBox endCity = (CheckBox)findViewById(R.id.checkBox_any_end_city);
        endCity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner endCitySpinner = (Spinner) findViewById(R.id.spinner_end_city);
                if(isChecked) {
                    endCitySpinner.setSelection(0);
                    endCitySpinner.setEnabled(false);
                }else{
                    endCitySpinner.setEnabled(true);
                }
            }
        });
    }

    private void initStartCityListener() {
        CheckBox startCity = (CheckBox)findViewById(R.id.checkBox_any_start_city);
        startCity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner startCitySpinner = (Spinner) findViewById(R.id.spinner_start_city);
                if(isChecked) {
                    startCitySpinner.setSelection(0);
                    startCitySpinner.setEnabled(false);
                }else{
                    startCitySpinner.setEnabled(true);
                }
            }
        });
    }

    private void initStartStateListener() {
        CheckBox startState = (CheckBox)findViewById(R.id.checkBox_any_start_state);
        startState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner startStateSpinner = (Spinner) findViewById(R.id.spinner_start_state);
                Spinner startCitySpinner = (Spinner) findViewById(R.id.spinner_start_city);
                CheckBox startCityCheckbox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                if(isChecked) {
                    startStateSpinner.setSelection(0);
                    startStateSpinner.setEnabled(false);
                    startCityCheckbox.setChecked(false);
                    startCityCheckbox.setEnabled(false);
                    startCitySpinner.setSelection(0);
                    startCitySpinner.setEnabled(false);
                }else{
                    startStateSpinner.setEnabled(true);
                    startCitySpinner.setEnabled(true);
                    startCityCheckbox.setEnabled(true);
                }
            }
        });
    }

    private void initEndStateListener() {
        CheckBox endState = (CheckBox)findViewById(R.id.checkBox_any_end_state);
        endState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner endStateSpinner = (Spinner) findViewById(R.id.spinner_end_state);
                Spinner endCitySpinner = (Spinner) findViewById(R.id.spinner_end_city);
                CheckBox endCityCheckbox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                if(isChecked) {
                    endStateSpinner.setSelection(0);
                    endStateSpinner.setEnabled(false);
                    endCityCheckbox.setChecked(false);
                    endCityCheckbox.setEnabled(false);
                    endCitySpinner.setSelection(0);
                    endCitySpinner.setEnabled(false);
                }else{
                    endStateSpinner.setEnabled(true);
                    endCitySpinner.setEnabled(true);
                    endCityCheckbox.setEnabled(true);
                }
            }
        });
    }

    private void setStartDateLabel() {
        TextView button = (TextView) findViewById(R.id.textView_date_label);
        button.setHint(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().start_date_search_vehicle));
        EditText edittext= (EditText) findViewById(R.id.editText_start_date);
        edittext.setText(Calendar.getInstance().toString());
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(SearchLoadActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        try {
            if(sdf.parse(sdf.format(myCalendar.getTime())).before(sdf.parse(sdf.format(Calendar.getInstance().getTime())))){
                AlertDialog.Builder builder = new AlertDialog.Builder(SearchLoadActivity.this);
                builder.setMessage(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().today_date_is_min_date));
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
                return;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        EditText edittext= (EditText) findViewById(R.id.editText_start_date);
        edittext.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateSearchLoadButtonText() {
        Button button = (Button)findViewById(R.id.button_search_load);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().search_load_label));
    }

    private void setLoadWeightLabel() {
        TextView button = (TextView) findViewById(R.id.textView_vehicle_capacity_label);
        button.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().load_weight));
    }

    private void getPreferredLanguage() {
        preferredLanguage = TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().LANGUAGE_PREFERRED_KEY);
    }

    private void updateFormLabelText() {
        TextView registerMeButton = (TextView)findViewById(R.id.TextView_search_result_label);
        registerMeButton.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().fill_search_load_form_label));
        registerMeButton.setTextColor(Color.BLACK);
    }

    private void updateStartCitySpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_start_city);
        String currentState = ((Spinner) findViewById(R.id.spinner_start_state)).getSelectedItem().toString();

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_start_city));

        if(!currentState.equals(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_start_state)))
            spinnerArray.addAll(SeedData.getInstance().stateCity.get(currentState));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
    }

    private void updateStartStateSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_start_state);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_start_state));

        for(Map.Entry<String, List<String>> entry : SeedData.getInstance().stateCity.entrySet()){
            spinnerArray.add(entry.getKey());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if(arg2 == 0){
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_start_city);
                    spinner.setVisibility(View.GONE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                    checkBox.setVisibility(View.GONE);
                }
                else{
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_start_city);
                    spinner.setVisibility(View.VISIBLE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_start_city);
                    checkBox.setVisibility(View.VISIBLE);
                }
                updateStartCitySpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void updateEndCitySpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
        String currentState = ((Spinner) findViewById(R.id.spinner_end_state)).getSelectedItem().toString();

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_city));

        if(!currentState.equals(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_state)))
            spinnerArray.addAll(SeedData.getInstance().stateCity.get(currentState));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);
    }

    private void updateEndStateSpinner() {
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_end_state);

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().select_destination_state));

        for(Map.Entry<String, List<String>> entry : SeedData.getInstance().stateCity.entrySet()){
            spinnerArray.add(entry.getKey());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if(arg2 == 0){
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
                    spinner.setVisibility(View.GONE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                    checkBox.setVisibility(View.GONE);
                }
                else{
                    Spinner spinner = (Spinner) findViewById(R.id.spinner_end_city);
                    spinner.setVisibility(View.VISIBLE);
                    CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_any_end_city);
                    checkBox.setVisibility(View.VISIBLE);
                }
                updateEndCitySpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private boolean areAllRequiredFieldsFilled() {
        if (((EditText) findViewById(R.id.editText_vehicle_capacity_required)).getText().toString().trim().length() != 0){
            return true;
        }

        return false;
    }

    private boolean allUserInputsAreValid() {
        TextView textView = (TextView)findViewById(R.id.TextView_search_result_label);

        if(!areAllRequiredFieldsFilled()){
            textView.setText(LanguageStrings.getInstance().fill_required_field_label.get(preferredLanguage));
            textView.setTextColor(Color.RED);
            return false;
        }

        if(!isValidVehicleCapacity()){
            textView.setText(TransporterSharedPreferences.getInstance().getPreferenceValue(LanguageStrings.getInstance().invalid_capacity_label));
            textView.setTextColor(Color.RED);
            return false;
        }

        return true;
    }

    private boolean isValidVehicleCapacity(){
        String perVehicleCapacity = ((EditText) findViewById(R.id.editText_vehicle_capacity_required)).getText().toString();
        try {
            float capacity = Float.parseFloat(perVehicleCapacity);
            return capacity > 0;
        }catch(Exception ex){
            return false;
        }
    }

    public void OnSearchLoadClick(View view){
        updateFormLabelText();
        if(allUserInputsAreValid()){
            try {
                searchLoad();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void searchLoad() throws Exception {
        String startDate = ((EditText) findViewById(R.id.editText_start_date)).getText().toString();

        int startState = ((Spinner) findViewById(R.id.spinner_start_state)).getSelectedItemPosition();
        int startCity = ((Spinner) findViewById(R.id.spinner_start_city)).getSelectedItemPosition();
        int endState = ((Spinner) findViewById(R.id.spinner_end_state)).getSelectedItemPosition();
        int endCity = ((Spinner) findViewById(R.id.spinner_end_city)).getSelectedItemPosition();
        String perVehicleCapacity = ((EditText) findViewById(R.id.editText_vehicle_capacity_required)).getText().toString();

        Trip trip = new Trip();
        trip.setStartDate(startDate);
        trip.setStartState(Integer.toString(startState));
        trip.setStartCity(Integer.toString(startCity));
        trip.setEndState(Integer.toString(endState));
        trip.setEndCity(Integer.toString(endCity));
        trip.setPerVehicleCapacity(perVehicleCapacity);

        String phoneNumber = TransporterSharedPreferences.getInstance().getPreferenceValue(CoreConstants.USER_REGISTER_ID);
        User user = new User(phoneNumber);

        UserTrip userTrip = new UserTrip(user, trip);
        (new SearchLoadActivity.searchLoad(SearchLoadActivity.this)).execute(userTrip);
    }

    private class searchLoad extends AsyncTask<UserTrip, Void, String> {
        private ProgressDialog pd;
        private Activity activity;

        private searchLoad(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected String doInBackground(UserTrip... userTrip) {
            String result = null;
            for(int index = 0; index < userTrip.length; index++){
                result = CoreAdapter.searchLoadForVehicle(userTrip[index]);
            }
            pd.dismiss();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != null
                    && LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) == null
                    && LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) == null){
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), SearchResultActivity.class);
                intent.putExtra(CoreConstants.SearchVehicleResult, result);
                startActivity(intent);

            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                if(result != null &&
                        LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result) != null &&
                        LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result) != null) {
                    if (preferredLanguage.equals("HINDI")) {
                        builder.setMessage(LanguageStrings.getInstance().HINDI_Error_Code_Map.get(result));
                    }else if (preferredLanguage.equals("ENGLISH")){
                        builder.setMessage(LanguageStrings.getInstance().ENGLISH_Error_Code_Map.get(result));
                    }
                }else{
                    builder.setMessage(LanguageStrings.getInstance().connection_error_toast.get(preferredLanguage));
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        LanguageStrings.getInstance().ok_label.get(preferredLanguage),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
